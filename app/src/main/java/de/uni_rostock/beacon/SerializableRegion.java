package de.uni_rostock.beacon;

import android.graphics.Path;
import android.graphics.Region;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.LinkedList;

/**
 * Created by bauerj on 26.02.2016.
 */
public class SerializableRegion extends Region implements Serializable {
    private static class Action implements Serializable {
    }
    private static class OpAction extends Action {
        private Region region;
        private Op op;

        public OpAction(Region region, Op op) {
            this.region = region;
            this.op = op;
        }
    }
    private static class SetPathAction extends Action {
        private Path path;
        private Region clip;

        public SetPathAction(Path path, Region clip) {
            this.path = new SerializablePath((SerializablePath)path);
            this.clip = clip;
        }
    }
    private static class SetAction extends Action {
        int left, top, right, bottom;

        public SetAction(int left, int top, int right, int bottom) {
            this.left = left;
            this.top = top;
            this.right = right;
            this.bottom = bottom;
        }
    }
    private LinkedList<Action> history = new LinkedList<>();

    @Override
    public boolean op(Region region, Op op) {
        history.add(new OpAction(region, op));
        return super.op(region, op);
    }

    @Override
    public boolean setPath(Path path, Region clip) {
        history.add(new SetPathAction(path, clip));
        return super.setPath(path, clip);
    }

    @Override
    public boolean set(int left, int top, int right, int bottom) {
        history.add(new SetAction(left, top, right, bottom));
        return super.set(left, top, right, bottom);
    }

    private void writeObject(ObjectOutputStream out) throws IOException {
        out.writeObject(history);
    }

    private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException{
        //noinspection unchecked
        history = (LinkedList<Action>) in.readObject();
        apply();
    }

    private void apply() {
        LinkedList<Action> actions = history;
        history = new LinkedList<>();
        for (Action a: actions) {
            if (a instanceof OpAction) {
                OpAction concrete = (OpAction) a;
                op(concrete.region, concrete.op);
            }
            else if (a instanceof SetPathAction) {
                SetPathAction concrete = (SetPathAction) a;
                setPath(concrete.path, concrete.clip);
            }
            else if (a instanceof SetAction) {
                SetAction concrete = (SetAction) a;
                set(concrete.left, concrete.top, concrete.right, concrete.bottom);
            }
        }
    }
}
