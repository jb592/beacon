package de.uni_rostock.beacon;

import com.orm.SugarApp;

import de.uni_rostock.beacon.bluetooth.BluetoothLocalization;

/**
 * @author Johann Bauer
 *         created 08.02.2016
 */
public class MyApplication extends SugarApp {
    public  static MyApplication application;
    private static BluetoothLocalization localization;
    public static MainActivity activity;

    public static ILocalization getLocalization() {
        if (localization == null)
            localization = new BluetoothLocalization();
        return localization;
    }

    public static MainActivity getActivity() {
        return activity;
    }

    @Override
    public void onCreate()
    {
        super.onCreate();
        application = this;
    }
}
