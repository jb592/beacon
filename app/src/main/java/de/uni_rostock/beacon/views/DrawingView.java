package de.uni_rostock.beacon.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.Region;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.Pair;
import android.view.Display;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import de.uni_rostock.beacon.MapData;
import de.uni_rostock.beacon.MyApplication;
import de.uni_rostock.beacon.Position;
import de.uni_rostock.beacon.R;
import de.uni_rostock.beacon.SerializablePath;
import de.uni_rostock.beacon.SerializableRegion;

/**
 * Based on DrawingView by Stephen Vinouze on 20/11/2015.
 * @see //github.com/StephenVinouze/DrawingView
 */
public class DrawingView extends View {

    private static final int kColor = Color.BLACK;
    private static final float kTouchTolerance = 5;
    private static final float kLineThickness = 5;


    private int maxHeight = 0;
    private Bitmap mBitmap;
    private Canvas mCanvas;
    private SerializablePath mPath;
    private Paint mBitmapPaint;
    private Paint mPaint;
    private Paint mFillPaint;
    private Paint mPointPaint;
    private float mX;
    private float mY;
    private float mTolerance = kTouchTolerance;
    private float mThickness = kLineThickness;
    private int mColor = kColor;
    private int mDelColor = kColor;
    private int mAddColor = kColor;
    private int mShapeColor = kColor;
    private Pair<Float, Float> start;
    private Button add;
    private Button remove;
    private Matrix mScaleMatrix = new Matrix();
    private MapData mapData;
    protected ScaleGestureDetector mScaleDetector;
    private Position positionToDraw;
    private Position positionToDraw2;
    private List<Region> regionsToDraw = new LinkedList<>();

    private int[] colors = {
            Color.argb(100, 62,114,19),
            Color.argb(100, 128,51,21),
            Color.argb(100, 18,54,82),
            Color.argb(100, 127,101,21),
            Color.argb(100, 49,20,86),
    };

    public void setPositionToDraw(Position positionToDraw) {
        this.positionToDraw = positionToDraw;
        invalidate();
    }

    public void setPositionToDraw2(Position positionToDraw2) {
        this.positionToDraw2 = positionToDraw2;
        invalidate();
    }

    public void setRemoveButton(Button remove) {
        this.remove = remove;
    }

    public void setAddButton(Button add) {
        this.add = add;
    }

    public void setEditableRegion(Region r) {
        region = (SerializableRegion) r;
        invalidate();
        redrawCache();
    }

    private int currentColor = 0;
    public void addRegionToDraw(Region region) {
        if (region == null)
            return;
        this.regionsToDraw.add(region);
        int old = mFillPaint.getColor();
        mFillPaint.setColor(colors[++currentColor % colors.length]);
        mCanvas.drawPath(region.getBoundaryPath(), mFillPaint);
        mFillPaint.setColor(old);
        invalidate();
    }

    private HashMap<Region, String> regionDescriptions = new HashMap<>();
    public void addRegionToDraw(Region region, String description) {
        regionDescriptions.put(region, description);
        addRegionToDraw(region);
    }

    public void redrawCache() {
        currentColor = 0;
        mCanvas.drawBitmap(mapData.getScaledBitmap(), 0, 0, mBitmapPaint);
        mCanvas.drawPath(region.getBoundaryPath(), mFillPaint);

        Rect border = new Rect(0, 0, mapData.getWidth() - 1, mapData.getHeight() - 1);
        Paint paint = new Paint();
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(10);
        paint.setColor(Color.BLACK);

        mCanvas.drawRect(border, paint);
        int old = mFillPaint.getColor();
        for (Region r: regionsToDraw) {
            mFillPaint.setColor(colors[++currentColor % colors.length]);
            mCanvas.drawPath(r.getBoundaryPath(), mFillPaint);
        }
        mFillPaint.setColor(old);
    }

    public void clearRegionsToDraw() {
        regionsToDraw.clear();
        mCanvas.drawBitmap(mapData.getScaledBitmap(), 0, 0, mBitmapPaint);
        mCanvas.drawPath(region.getBoundaryPath(), mFillPaint);
    }

    public void setMapData(MapData mapData) {
        this.mapData = mapData;
        mScaleMatrix = new Matrix();
        // gleich zum Anfang richtig skalieren
        float xScale = (float)getWidth() / mapData.getWidth();
        float yScale = (float)getHeight() / mapData.getHeight();
        float maxScale = (xScale > yScale) ? xScale : yScale;
        if (maxScale > 0)
            mScaleMatrix.postScale(maxScale, maxScale);
        int height = mapData.getHeight();
        if (((View)getParent()).getHeight()+1 > height)
            height = ((View)getParent()).getHeight()+1;
        mBitmap = Bitmap.createBitmap(mapData.getWidth(), height, Bitmap.Config.ARGB_8888);
        mCanvas = new Canvas(mBitmap);
        mCanvas.drawBitmap(mapData.getScaledBitmap(), 0, 0, mBitmapPaint);
        mCanvas.drawPath(region.getBoundaryPath(), mFillPaint);

        Rect border = new Rect(0, 0, mapData.getWidth() - 1, mapData.getHeight() - 1);
        Paint paint = new Paint();
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(10);
        paint.setColor(Color.BLACK);

        mCanvas.drawRect(border, paint);

        WindowManager wm = (WindowManager) MyApplication.getActivity().getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        if (size.y > getHeight()) {
            ViewGroup.LayoutParams p = getLayoutParams();
            p.height = size.y;
            p.width = size.x;
            setLayoutParams(p);
        }
        invalidate();
    }

    public Position getPositionToDraw() {
        return positionToDraw;
    }

    public Position getPositionToDraw2() {
        return positionToDraw2;
    }

    public enum Mode {
        DRAW_REGION_INITIAL,
        DRAW_REGION_ADD,
        DRAW_REGION_REMOVE,
        DRAW_POINT,
        DRAW_NONE
    }

    private Mode mode = Mode.DRAW_REGION_INITIAL;

    private SerializableRegion region = new SerializableRegion();

    public DrawingView(Context context) {
        super(context);
        initView();
    }

    public DrawingView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();

        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.DrawingView);

        final int N = a.getIndexCount();
        for (int i = 0; i < N; ++i) {

            int attr = a.getIndex(i);
            if (attr == R.styleable.DrawingView_drawingColor) {
                setDrawingColor(a.getColor(attr, kColor));
            }
            else if (attr == R.styleable.DrawingView_drawingThickness) {
                setDrawingThickness(a.getFloat(attr, kLineThickness));
            }
            else if (attr == R.styleable.DrawingView_drawingTolerance) {
                setDrawingTolerance(a.getFloat(attr, kTouchTolerance));
            }
        }

        mShapeColor = Color.argb(60, 10, 15, 200);
        mDelColor = Color.argb(60, 200, 15, 20);
        mAddColor = Color.argb(60, 0, 210, 10);

        mPointPaint = new Paint();
        mPointPaint.setAntiAlias(true);
        mPointPaint.setDither(true);
        mPointPaint.setStyle(Paint.Style.STROKE);
        mPointPaint.setStrokeJoin(Paint.Join.ROUND);
        mPointPaint.setStrokeCap(Paint.Cap.ROUND);
        mPointPaint.setColor(Color.argb(200, 250, 100, 0));
        mPointPaint.setStrokeWidth(50l);
        a.recycle();

        mScaleDetector = new ScaleGestureDetector(context, new ScaleGestureDetector.OnScaleGestureListener() {
            private float lastFocusX, lastFocusY;

            @Override
            public boolean onScale(ScaleGestureDetector detector) {
                Matrix transformationMatrix = new Matrix();
                float focusX = detector.getFocusX();
                float focusY = detector.getFocusY();

                //Zoom focus is where the fingers are centered,
                transformationMatrix.postTranslate(-focusX, -focusY);

                transformationMatrix.postScale(detector.getScaleFactor(), detector.getScaleFactor());
                // Mit 2 Fingern scrollen
                float focusShiftX = focusX - lastFocusX;
                float focusShiftY = focusY - lastFocusY;
                transformationMatrix.postTranslate(focusX + focusShiftX, focusY + focusShiftY);

                lastFocusX = focusX;
                lastFocusY = focusY;

                Matrix test = new Matrix();
                test.set(mScaleMatrix);
                test.postConcat(transformationMatrix);
                // Prüfen, ob wir das Bild auch nicht zu klein machen
                if (mapData != null && (test.mapRadius(mapData.getWidth()) < 0.5*getWidth() && test.mapRadius(mapData.getHeight()) < 0.5*getHeight()) && detector.getScaleFactor() < 1) {
                    return true;
                }
                mScaleMatrix.postConcat(transformationMatrix);
                invalidate();
                return true;
            }

            @Override
            public boolean onScaleBegin(ScaleGestureDetector detector) {
                lastFocusX = detector.getFocusX();
                lastFocusY = detector.getFocusY();
                return true;
            }

            @Override
            public void onScaleEnd(ScaleGestureDetector detector) {

            }
        });

    }

    public void setMode(Mode m) {

        mode = m;
        if (add == null || remove == null)
            return;
        if (mode == Mode.DRAW_REGION_ADD) {
            add.setTypeface(Typeface.DEFAULT_BOLD);
            remove.setTypeface(Typeface.DEFAULT);
        }
        if (mode == Mode.DRAW_REGION_REMOVE) {
            remove.setTypeface(Typeface.DEFAULT_BOLD);
            add.setTypeface(Typeface.DEFAULT);
        }
        if (mode == Mode.DRAW_REGION_INITIAL) {
            remove.setEnabled(false);
            add.setEnabled(false);
        }
        else {
            remove.setEnabled(true);
            add.setEnabled(true);
        }
    }


    public int getDrawingColor() {
        return mColor;
    }

    public void setDrawingColor(int color) {
        mColor = color;
        mPaint.setColor(color);
    }

    public float getThickness() {
        return mThickness;
    }

    public void setDrawingThickness(float thickness) {
        mThickness = thickness;
        mPaint.setStrokeWidth(thickness);
    }

    public float getTolerance() {
        return mTolerance;
    }

    public void setDrawingTolerance(float tolerance) {
        mTolerance = tolerance;
    }

    /**
     * Reset the drawing view by cleaning the canvas that contains the drawing
     */
    public void resetDrawing() {
        mCanvas.drawBitmap(mapData.getScaledBitmap(), 0, 0, mBitmapPaint);
        setMode(Mode.DRAW_REGION_INITIAL);
        invalidate();
    }

    /**
     * Let you retrieve the drawing that has been drawn inside the canvas
     * @return The drawing as a Bitmap
     */
    public Bitmap getDrawing() {
        Bitmap b = Bitmap.createBitmap(getWidth(), getHeight(), Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(b);
        layout(getLeft(), getTop(), getRight(), getBottom());
        draw(c);

        return b;
    }

    public SerializableRegion getRegion() {
        return region;
    }

    private void initView() {
        mPath = new SerializablePath();
        mBitmapPaint = new Paint(Paint.DITHER_FLAG);
        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setDither(true);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeJoin(Paint.Join.ROUND);
        mPaint.setStrokeCap(Paint.Cap.ROUND);
        mPaint.setStrokeWidth(mThickness);

        mFillPaint = new Paint();
        mFillPaint.setColor(Color.argb(100, 55,66,77));
        mFillPaint.setAntiAlias(true);
        mFillPaint.setDither(true);
        mFillPaint.setStyle(Paint.Style.FILL);

    }

    private void startTouch(float x, float y) {
        switch (mode) {
            case DRAW_REGION_REMOVE:
                mColor = mDelColor;
                break;
            case DRAW_REGION_ADD:
                mColor = mAddColor;
        }
        mPaint.setColor(mColor);
        mPath.reset();
        mPath.moveTo(x, y);
        mX = x;
        mY = y;
        start = new Pair<>(x, y);
    }

    private void moveTouch(float x, float y) {
        float dx = Math.abs(x - mX);
        float dy = Math.abs(y - mY);
        if (dx >= mTolerance || dy >= mTolerance) {
            mPath.quadTo(mX, mY, (x + mX) / 2, (y + mY) / 2);
            mX = x;
            mY = y;
        }
    }

    private void endTouch() {
        if (!mPath.isEmpty()) {
            mPath.lineTo(mX, mY);
            mPath.lineTo(start.first, start.second);
            SerializableRegion clip = new SerializableRegion();
            clip.set(0, 0, mapData.getWidth(), mapData.getHeight());
            SerializableRegion snd = new SerializableRegion();
            switch (mode) {
                case DRAW_REGION_INITIAL:
                    region.setPath(mPath, clip);
                    setMode(Mode.DRAW_REGION_ADD);
                    break;
                case DRAW_REGION_ADD:
                    snd.setPath(mPath, clip);
                    region.op(snd, SerializableRegion.Op.UNION);
                    break;
                case DRAW_REGION_REMOVE:
                    snd.setPath(mPath, clip);
                    region.op(snd, SerializableRegion.Op.DIFFERENCE);
            }
            mCanvas.drawBitmap(mapData.getScaledBitmap(), 0, 0, mBitmapPaint);
            mPaint.setStyle(Paint.Style.FILL);

            mPaint.setColor(mShapeColor);
            mCanvas.drawPath(region.getBoundaryPath(), mFillPaint);
            mPaint.setStyle(Paint.Style.STROKE);
        } else {
            mCanvas.drawPoint(mX, mY, mPaint);
        }

        mPath.reset();
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

        WindowManager wm = (WindowManager) MyApplication.getActivity().getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        mBitmap = Bitmap.createBitmap(w, size.y + 1, Bitmap.Config.ARGB_8888);
        mCanvas = new Canvas(mBitmap);
        mCanvas.drawPath(region.getBoundaryPath(), mFillPaint);
        if (mapData != null)
            setMapData(mapData);
        if (maxHeight < (h > 0 ? h : ((View)getParent()).getHeight()+1))
            maxHeight = (h > 0 ? h : ((View)getParent()).getHeight()+1);


    }

    private Region clickedRegion = null;

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        mCanvas.drawPath(mPath, mPaint);
        canvas.drawBitmap(mBitmap, mScaleMatrix, mBitmapPaint);

        if (region != null)
            canvas.drawPath(region.getBoundaryPath(), mPaint);

        if (positionToDraw != null && mScaleMatrix != null) {
            float[] points = {positionToDraw.getX(), positionToDraw.getY()};
            mScaleMatrix.mapPoints(points);
            mPointPaint.setStrokeWidth(mScaleMatrix.mapRadius(25.0f));
            canvas.drawPoint(points[0], points[1], mPointPaint);
        }

        if (positionToDraw2 != null && mScaleMatrix != null) {
            float[] points = {positionToDraw2.getX(), positionToDraw2.getY()};
            mScaleMatrix.mapPoints(points);
            mPointPaint.setStrokeWidth(mScaleMatrix.mapRadius(25.0f));

            int i = mPointPaint.getColor();
            mPointPaint.setColor(Color.BLUE);

            canvas.drawPoint(points[0], points[1], mPointPaint);
            mPointPaint.setColor(i);
        }

        if (clickedRegion != null) {
            Rect bounds = clickedRegion.getBounds();
            float points[] = {bounds.centerX(), bounds.centerY()};
            mScaleMatrix.mapPoints(points);
            mPaint.setTextSize(70);
            int old = mPaint.getColor();

            String description = regionDescriptions.get(clickedRegion);
            if (description != null) {
                mPaint.setStyle(Paint.Style.FILL);
                canvas.drawText(description, points[0], points[1], mPaint);
                mPaint.setStyle(Paint.Style.STROKE);
                mPaint.setColor(Color.YELLOW);
                canvas.drawText(description, points[0], points[1], mPaint);
                mPaint.setColor(old);
            }

        }

    }



    private Region lastClickedRegion;
    @Override
    public boolean onTouchEvent(MotionEvent e) {
        super.onTouchEvent(e);
        float[] points = {e.getX(), e.getY()};
        Matrix invers = new Matrix();
        mScaleMatrix.invert(invers);
        invers.mapPoints(points);
        // Falls der Stift benutzt wird:
        // if (true) {
        if (e.getDevice() != null && e.getDevice().getName() != null && e.getDevice().getName().contains("pen")) {
            if (mapData == null)
                return false;
            if (mode == Mode.DRAW_NONE) {
                Toast.makeText(MyApplication.getActivity(), "Auf dieser Karte können Sie nicht zeichnen.", Toast.LENGTH_LONG).show();
                return false;
            }

            if (mode == Mode.DRAW_POINT) {
                positionToDraw = new Position((int)points[0], (int)points[1]);
                invalidate();
                return true;
            }
            switch (e.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    startTouch(points[0], points[1]);
                    invalidate();
                    break;
                case MotionEvent.ACTION_MOVE:
                    moveTouch(points[0], points[1]);
                    invalidate();
                    break;
                case MotionEvent.ACTION_UP:
                    endTouch();
                    invalidate();
                    break;
            }
            return true;
        }
        if (mode == Mode.DRAW_NONE) {
            clickedRegion = null;
            for (Region r: regionsToDraw) {
                if (r.contains((int)points[0], (int)points[1])) {
                    clickedRegion = r;
                    if (lastClickedRegion != clickedRegion) {
                        redrawCache();
                        Path p = clickedRegion.getBoundaryPath();
                        mCanvas.drawPath(p, mFillPaint);
                        mCanvas.drawPath(p, mPaint);
                    }
                    lastClickedRegion = r;
                    break;
                }
            }
            if (clickedRegion == null && clickedRegion != lastClickedRegion) {
                lastClickedRegion = null;
                redrawCache();
            }
        }
        mScaleDetector.onTouchEvent(e);

        if (e.getPointerCount() == 1) {
            switch (e.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    startSwipe(e.getX(), e.getY());
                    invalidate();
                    break;
                case MotionEvent.ACTION_MOVE:
                    moveSwipe(e.getX(), e.getY());
                    invalidate();
                    break;
                case MotionEvent.ACTION_UP:
                    endSwipe();
                    invalidate();
                    break;
            }
        }
        return true;

    }

    private PointF swipeAt;
    private void startSwipe(float x, float y) {
        if (swipeAt == null)
            swipeAt = new PointF(x, y);
    }


    private void moveSwipe(float x, float y) {
        // empirisch bestimmt
        final int MAX_DELTA = 150;
        if (Math.abs(x - swipeAt.x) > MAX_DELTA ||  Math.abs(y - swipeAt.y) > MAX_DELTA) {
            swipeAt.set(x, y);
            return;
        }
        mScaleMatrix.postTranslate(x - swipeAt.x, y - swipeAt.y);
        swipeAt.set(x, y);
    }

    private void endSwipe() {
        swipeAt = null;
    }


}