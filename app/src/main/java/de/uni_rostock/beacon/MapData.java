package de.uni_rostock.beacon;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.util.DisplayMetrics;
import android.util.Pair;
import android.view.WindowManager;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by georgehrke on 2/6/16.
 */
public class MapData implements Comparable<MapData> {

    /**
     * Name of current map
     */
    private String name;

    private File image;

    public File getImage() {
        return image;
    }

    public void setImage(File image) {
        this.image = image;
    }

    private Bitmap scaledBitmap;
    private Pair<Integer, Integer> mapDimension;

    /**
     * Gibt die Bilddatei der Karte zurück und skaliert sie vorher gegebenenfalls
     *
     * @return      Skalierte Bitmap der Karte
     */
    public Bitmap getScaledBitmap() {
        if (scaledBitmap != null)
            return scaledBitmap;
        if (getImage() != null) {
            // In Android vor Version 4.3 werden Bitmaps nicht in der VM gespeichert,
            // weshalb der GC selbst gestartet werden muss.
            System.gc();
            // Wenn man größere Bilder lädt, können schon mal ein paar 100MB an Arbeitsspeicher anfallen
            // Um das zu verhindern wird das Bild schon beim Einlesen herunterskaliert.
            BitmapFactory.Options options = new BitmapFactory.Options();
            // Nur Auflösung aus der Quelle laden
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(getImage().getAbsolutePath(), options);
            int imageWidth = options.outWidth;
            int imageHeight = options.outHeight;
            // Bildschirmgröße ermitteln
            DisplayMetrics displayMetrics = new DisplayMetrics();
            WindowManager wm = (WindowManager) MyApplication.application.getSystemService(Context.WINDOW_SERVICE); // the results will be higher than using the activity context object or the getWindowManager() shortcut
            wm.getDefaultDisplay().getMetrics(displayMetrics);
            int maxRes = displayMetrics.widthPixels;
            if (displayMetrics.heightPixels > maxRes)
                maxRes = displayMetrics.heightPixels;
            options = new BitmapFactory.Options();
            if (imageWidth > maxRes) {
                options.inSampleSize = (imageWidth / maxRes);
                height = imageHeight / options.inSampleSize;
                width = imageWidth / options.inSampleSize;
            }
            else {
                height = imageHeight;
                width = imageWidth;
            }
            scaledBitmap = BitmapFactory.decodeFile(getImage().getAbsolutePath(), options);
            if (options.inSampleSize > 1) {
                // Die Skalierung dauert relativ lange, daher ist es besser die skalierte Version direkt zu speichern
                // Um beim nächsten Laden Zeit zu sparen
                FileOutputStream out = null;
                try {
                    out = new FileOutputStream(getImage());
                    scaledBitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
                    // PNG is a lossless format, the compression factor (100) is ignored
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    try {
                        if (out != null) {
                            out.close();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
            return scaledBitmap;
        }
        return null;
    }

    public Pair<Integer, Integer> getMapDimension() {
        if (mapDimension == null) {
            Bitmap bitmap = BitmapFactory.decodeFile(getImage().getAbsolutePath());
            mapDimension = new Pair<>(bitmap.getWidth(), bitmap.getHeight());
        }
        return mapDimension;
    }

    public MapData() {
        this.name = "";
    }

    public MapData(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private int height = -1;
    private int width = -1;
    public int getHeight() {
        if (height < 0) {
            getScaledBitmap();
        }
        return height;
    }

    public int getWidth() {
        if (width < 0) {
            getScaledBitmap();
        }
        return width;
    }

    public String toString() {
        return name;
    }


    @Override
    public int compareTo(@NonNull MapData another) {
        return name.compareTo(another.name);
    }
}
