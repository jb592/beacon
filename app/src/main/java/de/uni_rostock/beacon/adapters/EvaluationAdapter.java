package de.uni_rostock.beacon.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import java.util.List;

import de.uni_rostock.beacon.MyApplication;
import de.uni_rostock.beacon.R;
import de.uni_rostock.beacon.TestRun;
import de.uni_rostock.beacon.fragments.EvaluationFragmentMain;

public class EvaluationAdapter extends RecyclerView.Adapter {
    private List<TestRun> testRuns;
    public EvaluationAdapter(List<TestRun> places) {
        this.testRuns = places;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_evaluations, parent, false);
        PlacesViewHolder pvh = new PlacesViewHolder(v);
        return pvh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        ((PlacesViewHolder)holder).name.setText(testRuns.get(position).getName());
        ((PlacesViewHolder)holder).count.setText("Anzahl der Testorte: " + testRuns.get(position).getTestPlaces().size());
        ((PlacesViewHolder)holder).accuracy.setText(testRuns.get(position).getAccuracy() + "%");
        ((PlacesViewHolder)holder).item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EvaluationFragmentMain.detailFragment.setTestRun(testRuns.get(position));
                MyApplication.getActivity().showFragment(EvaluationFragmentMain.detailFragment);
            }
        });
    }

    @Override
    public int getItemCount() {
        return testRuns.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public static class PlacesViewHolder extends RecyclerView.ViewHolder {
        TextView name;
        TextView count;
        TextView newName;
        View item;
        TextView accuracy;

        PlacesViewHolder(View itemView) {
            super(itemView);
            item = itemView;
            name = (TextView) itemView.findViewById(R.id.name);
            count = (TextView) itemView.findViewById(R.id.count);
            accuracy = (TextView) itemView.findViewById(R.id.accuracy);
        }
    }
}