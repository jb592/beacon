package de.uni_rostock.beacon.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import de.uni_rostock.beacon.MapData;
import de.uni_rostock.beacon.MyApplication;
import de.uni_rostock.beacon.R;
import de.uni_rostock.beacon.fragments.MapListFragment;

public class MapsAdapter extends RecyclerView.Adapter {
    private List<MapData> places;
    public MapsAdapter(List<MapData> places) {
        this.places = places;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_places, parent, false);
        PlacesViewHolder pvh = new PlacesViewHolder(v);
        return pvh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        ((PlacesViewHolder)holder).name.setText(places.get(position).getName());
        ((PlacesViewHolder)holder).name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyApplication.getActivity().showFragment(MapListFragment.detailFragment);
                MapListFragment.detailFragment.setMapData(places.get(position));

            }
        });
    }

    @Override
    public int getItemCount() {
        return places.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public static class PlacesViewHolder extends RecyclerView.ViewHolder {
        TextView name;

        PlacesViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.name);

        }
    }
}
