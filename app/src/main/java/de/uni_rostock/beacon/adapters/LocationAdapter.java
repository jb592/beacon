package de.uni_rostock.beacon.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import de.uni_rostock.beacon.Location;
import de.uni_rostock.beacon.MyApplication;
import de.uni_rostock.beacon.R;
import de.uni_rostock.beacon.fragments.LocationListFragment;

public class LocationAdapter extends RecyclerView.Adapter {
    private List<Location> locations;
    public LocationAdapter(List<Location> locations) {
        this.locations = locations;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_locations, parent, false);
        PlacesViewHolder pvh = new PlacesViewHolder(v);
        return pvh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        ((PlacesViewHolder)holder).name.setText(locations.get(position).getName());
        ((PlacesViewHolder)holder).mapName.setText(locations.get(position).getMapName());
        ((PlacesViewHolder)holder).item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyApplication.getActivity().showFragment(LocationListFragment.detailFragment);
                LocationListFragment.detailFragment.setLocation(locations.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        return locations.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public static class PlacesViewHolder extends RecyclerView.ViewHolder {
        TextView name;
        TextView mapName;
        View item;

        PlacesViewHolder(View itemView) {
            super(itemView);
            item = itemView;
            name = (TextView) itemView.findViewById(R.id.name);
            mapName = (TextView) itemView.findViewById(R.id.map_name);
        }
    }
}
