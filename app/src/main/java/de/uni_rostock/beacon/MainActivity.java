package de.uni_rostock.beacon;


import android.app.AlertDialog;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.SectionDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;

import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import de.uni_rostock.beacon.fragments.EvaluationFragmentMain;
import de.uni_rostock.beacon.fragments.Fragment;
import de.uni_rostock.beacon.fragments.LocationListFragment;
import de.uni_rostock.beacon.fragments.MapListFragment;
import de.uni_rostock.beacon.fragments.ProjectFragment;
import de.uni_rostock.beacon.fragments.ShowPositionFragment;

/**
 * @author Johann Bauer
 *         created 17.01.2016
 */
public class MainActivity extends FragmentActivity {
    Fragment[] fragments = {
            new ShowPositionFragment(),
            new LocationListFragment(),
            new EvaluationFragmentMain(),
            new MapListFragment(),
            new ProjectFragment(),
    };
    private final FragmentManager fragmentManager = getFragmentManager();
    private Fragment currentFragment;
    private Toolbar toolbar;
    private IDrawerItem currentDrawerItem;
    private Drawer drawer;
    private Menu menu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.app_name);
        menu = toolbar.getMenu();
        toolbar.setTitleTextColor(Color.WHITE);
        toolbar.setSubtitleTextColor(Color.WHITE);
        MyApplication.activity = this;
        DrawerBuilder builder = new DrawerBuilder()
                .withActivity(this)
                .withDisplayBelowStatusBar(true)
                .withFireOnInitialOnClick(true)
                .withSavedInstance(savedInstanceState)
                .withCloseOnClick(true)
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                        // do something with the clicked item :D
                        if (drawerItem == null)
                            return false;
                        currentDrawerItem = drawerItem;
                        Fragment f = (Fragment) drawerItem.getTag();
                        showFragment(f);
                        return false;
                    }
                });
        List<Fragment> collectedFragments = MyApplication.getLocalization().getAdditionalFragments();
        collectedFragments.addAll(Arrays.asList(fragments));
        TreeMap<String, LinkedList<Fragment>> categories = new TreeMap<>();
        for (Fragment i: collectedFragments) {
            if (!categories.containsKey(i.getCategory())) {
                categories.put(i.getCategory(), new LinkedList<Fragment>());
            }
            LinkedList<Fragment> c = categories.get(i.getCategory());
            c.add(i);
        }
        for (Map.Entry<String, LinkedList<Fragment>> i: categories.entrySet()) {
            Collections.sort(i.getValue());
            builder.addDrawerItems(new SectionDrawerItem().withDivider(true).withName(i.getKey()).withTag(i.getValue().get(0)));
            for (Fragment x: i.getValue())
                builder.addDrawerItems(new PrimaryDrawerItem().withName(" " + x.getName()).withTag(x));
        }
        //for (Fragment i: collectedFragments)
            //builder.addDrawerItems(new PrimaryDrawerItem().withName(i.getName()).withTag(i));
        drawer = builder
                .withToolbar(toolbar)
                .withActionBarDrawerToggle(true).build();

        //showFragment(fragments[0]);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        //if (currentDrawerItem != null)
        //    drawer.setSelection(currentDrawerItem);
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    public void showFragment(final Fragment f) {
        if (f == null)
            return;
        if (currentFragment != null && currentFragment.needsSave()) {
            new AlertDialog.Builder(this)
                    .setTitle("Änderungen verwerfen")
                    .setMessage("Möchten Sie fortfahren und alle ungespeicherten Änderungen verwerfen?")
                    .setPositiveButton("Ja", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            currentFragment = f;
                            menu.clear();
                            f.onInsertMenu(menu);
                            fragmentManager.beginTransaction().replace(R.id.fragment_container, f).commit();
                            toolbar.setTitle("Beacon Evaluator: " + f.getName());
                        }
                    })
                    .setNegativeButton("Nein", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // do nothing
                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
            return;
        }
        currentFragment = f;
        menu.clear();
        f.onInsertMenu(menu);
        fragmentManager.beginTransaction().replace(R.id.fragment_container, f).commit();
        toolbar.setTitle("Beacon Evaluator: " + f.getName());
    }

    public void setTitle(String t) {
        toolbar.setTitle("BeaconEvaluator: " + t);
    }

    @Override
    public void onBackPressed() {
        if (!currentFragment.onBackButtonPress()) {
            if (!drawer.isDrawerOpen())
                drawer.openDrawer();
            else
                super.onBackPressed();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        MyApplication.getLocalization().start();
    }

    @Override
    protected void onPause() {
        super.onPause();
        MyApplication.getLocalization().stop();
    }
}
