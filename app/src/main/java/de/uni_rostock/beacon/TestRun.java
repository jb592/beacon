package de.uni_rostock.beacon;

import com.orm.SugarRecord;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

/**
 * Created by Tom on 14.02.2016.
 */
public class TestRun extends SugarRecord {
    private List<TestPlace> testPlaces = new LinkedList<>();
    String name = "unnamed";
    String mapName = "";
    Date startDate = new Date();
    Date endDate;

    public TestRun()
    {

    }



    public TestRun(String testlaufName, String testMap)
    {
        name = testlaufName;
        mapName = testMap;
    }

    public Date getEndDate() {
        return endDate;
    }
    public Date getStartDate() {
        return startDate;
    }
    public Date getDate() {
        return getStartDate();
    }

    public void setEndDate() {
        this.endDate = new Date();
    }

    public void setMapName(String newName)
    {
        mapName = newName;
    }
    public String getMapName()
    {
        return mapName;
    }

    public void setName(String newName)
    {
        name = newName;
    }
    public String getName()
    {
        return name;
    }

    /**
     * Berechnet die aktuelle Gesamtgenauigkeit aller Testorte und gibt sie als Integerwert zurück
     *
     * @return      Genauigkeit des Testlaufs als Integer zwischen 0 und 100
     */
    public int getAccuracy()
    {
        int accuracy = 0;
        if(testPlaces.size() == 0) return accuracy;

        int positivePlaces = 0;
        for (TestPlace tp: testPlaces)
        {
            if(tp.isSameLocation()) positivePlaces++;
        }
        accuracy = (int)((float)positivePlaces / testPlaces.size() * 100);
        return accuracy;
    }

    /**
     * Fügt dem Testlauf einen neuen Testort hinzu und speichert diesen in der Datenbank
     *
     * @param  ort  Testort, der hinzugefügt werden soll
     * @return      void
     */
    void setTestPlace(TestPlace ort)
    {
        ort.save();
        testPlaces.add(ort);
    }

    /**
     * Gibt eine Liste aller Testorte zurück, die diesem Testlauf zugeordnet sind
     * Vorher werden noch alle Testorte aus der Datenbank ausgelesen und in ihren jeweiligen Testlauf zugeordnet
     *
     * @return      Alle Testorte dieses Testlaufs
     */
    public List<TestPlace> getTestPlaces()
    {
        List<TestPlace> p = TestPlace.find(TestPlace.class, "run = ?", String.valueOf(getId()));
        for (TestPlace x: p) {
            boolean contains = false;
            for (TestPlace y: testPlaces) {
                if (Objects.equals(y.getId(), x.getId()))
                    contains = true;
            }
            if (!contains)
                testPlaces.add(x);
        }
        return testPlaces;
    }

}
