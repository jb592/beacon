package de.uni_rostock.beacon;

import com.orm.SugarRecord;

import java.util.Objects;

/**
 * Created by georgehrke on 2/6/16.
 */
public class Position extends SugarRecord {

    private int x;

    private int y;

    private String mapName;

    public String getMapName() {
        return mapName;
    }

    public void setMapName(String mapName) {
        this.mapName = mapName;
    }

    public Position() {

    }

    public Position(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    @Override
    public String toString() {
        return mapName + "("+x+","+y+")";
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Position))
            return false;
        Position p = (Position)o;
        return x == p.x && y == p.y && Objects.equals(mapName, p.getMapName());
    }
}
