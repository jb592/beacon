package de.uni_rostock.beacon;

import android.media.MediaScannerConnection;
import android.os.Environment;
import android.util.JsonReader;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * Created by georgehrke on 2/18/16.
 */
public class MapDataManager {
    private Gson gson = new GsonBuilder().create();
    private File mFolder;

    public static final String path = Environment.getExternalStorageDirectory() + "/beacon/maps/";

    public MapDataManager() {
        mFolder = new File(path);
        if (!mFolder.exists()) {
            mFolder.mkdir();
            mFolder.setReadable(true);
            MediaScannerConnection.scanFile(
                    MyApplication.application, new String[]{mFolder.toString()}, null, null);
        }
    }

    /**
     * Gibt eine Auflistung aller momentan gespeicherten MapDatas zurück
     *
     * @return      Eine Liste bestehend aus den MapDatas aller momentan bekannten Karten
     */
    public List<MapData> getMapDatas() {
        File files[] = mFolder.listFiles();
        Set<MapData> entries = new HashSet<>();

        for (File file: files) {
            String name = file.getName();
            // remove file ending
            name = name.substring(0, name.lastIndexOf('.'));
            entries.add(getMapData(name));
        }
        List<MapData> l = new LinkedList<>();
        l.addAll(entries);
        Collections.sort(l);
        return l;
    }

    /**
     * Gibt die MapData zu einem Kartennamen zurück
     *
     * @param  name  Name der Karte, nach der gesucht werden soll
     * @return      MapData der gesuchten Karte
     */
    public MapData getMapData(String name) {
        File file = new File(path + name + ".json");
        MapData mapData = new MapData(name);
        /*if (file.exists()) {
            Position[][] positions;
            try {
                FileReader fReader = new FileReader(file);
                positions = gson.fromJson(fReader, Position[][].class);
            } catch(FileNotFoundException ex) {
                return null;
            }



            for (Position[] positionGroup: positions) {
                if (positionGroup == null) {
                    continue;
                }

                mapData.addWall(new Wall(positionGroup[0], positionGroup[1]));
            }
        }*/
        File image = new File(path + name + ".png");
        if (image.exists()) {
            mapData.setImage(image);
        }
        return mapData;
    }
}
