package de.uni_rostock.beacon;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Tom on 14.02.2016.
 */
public class EvaluationController {

    public List<TestRun> testRuns = TestRun.listAll(TestRun.class);
    // public static List<TestRun> testRuns = loadTestRuns();

    /**
     * Lädt alle Testläufe neu aus der "Datenbank" heraus und speichert sie unter testRuns
     * Diese Funktion wird aufgerufen, wenn ein anderes Projekt geladen wird, damit die dortigen Testläufe auch richtig geladen werden.
     *
     * @return      void
     */
    public void reloadData()
    {
        testRuns = TestRun.listAll(TestRun.class);
        aktuellerTestRun.delete();
        aktuellerTestRun = new TestRun();
    }

    TestRun aktuellerTestRun = new TestRun();

    /*public static List<TestRun> loadTestRuns()
    {
        testRuns = new LinkedList<>();

        List<TestRun> loadedTestRuns = TestRun.listAll(TestRun.class);
        for (TestRun t: loadedTestRuns) {
            testRuns.add(t);
        }

        return testRuns;
    }*/

    public EvaluationController()
    {

    }

    /**
     * Wird aufgerufen, wenn ein neuer Testlauf gestartet werden soll
     * Die aktuellerTestRun - Variable wird dazu mit der entsprechenden Karte und einem Beispielnamen initialisiert
     *
     * @param  mapName  Stringname der Map, auf der der Testlauf durchgeführt wird
     * @return      void
     */
    public void startTestRun(String mapName)
    {
        aktuellerTestRun = new TestRun("Neuer Testlauf", mapName);
    }

    /**
     * Fügt dem aktuellen Testlauf (aktuellerTestRun) einen neuen Testort hinzu
     *
     * @param  testPlace  Testort, der an das Ende der Liste mit den Testorten angehängt werden soll
     * @return      void
     */
    public void addTestPlace(TestPlace testPlace)
    {
        if(aktuellerTestRun == null) return;
        aktuellerTestRun.setTestPlace(testPlace);
    }

    /**
     * Wird aufgerufen, wenn ein der aktuelle Testlauf abgebrochen werden soll
     * Diese Funktion löscht den aktuellen Testlauf aus der Datenbank und initialisiert ihn neu
     *
     * @return      void
     */
    public void abortTestRun()
    {
        if(aktuellerTestRun == null) return;
        aktuellerTestRun.delete();
        aktuellerTestRun = null;
    }

    /**
     * Wird aufgerufen, der aktuelle Testlauf erfolgreich beendet wurde und gespeichert werden soll
     * Das Enddatum des Testlaufs wird gesetzt und er wird in die lokale Liste der Testläufe eingetragen, sowie in der Datenbank gespeichert
     *
     * @return      void
     */
    public void finishTestRun()
    {
        if(aktuellerTestRun == null) return;
        aktuellerTestRun.setEndDate();
        testRuns.add(aktuellerTestRun);
        aktuellerTestRun.save();
        aktuellerTestRun = null;
    }

    /**
     * Löscht einen Testlauf samt Testorten aus der Datenbank
     *
     * @param  removingRun  Testlauf, der gelöscht werden soll
     * @return      void
     */
    public void removeTestRun(TestRun removingRun)
    {
        if(testRuns.contains(removingRun)) {
            testRuns.remove(removingRun);
            removingRun.delete();
        }
    }

    public List<TestRun> getTestRuns()
    {
        return testRuns;
    }

    public TestRun getAktuellerTestRun()
    {
        aktuellerTestRun.save();
        return aktuellerTestRun;
    }
}
