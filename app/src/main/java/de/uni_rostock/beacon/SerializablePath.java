package de.uni_rostock.beacon;

import android.graphics.Path;


import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.LinkedList;

/**
 * Created by bauerj on 25.02.2016.
 */
public class SerializablePath extends Path implements Serializable {
    public static class Entry implements Serializable {
        private float a, b, c, d;
        public Entry() {}

        public Entry(float a, float b, float c, float d) {
            this.a = a;
            this.b = b;
            this.c = c;
            this.d = d;
        }
    }
    public LinkedList<Entry> moves = new LinkedList<>();

    public SerializablePath(SerializablePath src) {
        super(src);
        this.moves = new LinkedList<>(src.moves);
    }

    public SerializablePath() {
    }

    @Override
    public void moveTo(float x, float y) {
        super.moveTo(x, y);
        moves.add(new Entry(x,y,0,0));
    }

    @Override
    public void quadTo(float x1, float y1, float x2, float y2) {
        super.quadTo(x1, y1, x2, y2);
        moves.add(new Entry(x1, y1, x2, y2));
    }

    private void apply() {
        LinkedList<Entry> entries = this.moves;
        this.moves = new LinkedList<>();
        Entry e = entries.pop();
        moveTo(e.a, e.b);
        while (!entries.isEmpty()) {
            e = entries.pop();
            quadTo(e.a, e.b, e.c, e.d);
        }
    }

    private void writeObject(ObjectOutputStream out) throws IOException {
         out.writeObject(moves);
    }

    private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException{
        //noinspection unchecked
        moves = (LinkedList<Entry>) in.readObject();
        apply();
    }

    @Override
    public void reset() {
        moves.clear();
        super.reset();
    }
}
