package de.uni_rostock.beacon.bluetooth.fragments;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.melnykov.fab.FloatingActionButton;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import de.uni_rostock.beacon.MyApplication;
import de.uni_rostock.beacon.R;
import de.uni_rostock.beacon.bluetooth.Logger;
import de.uni_rostock.beacon.bluetooth.adapters.LocalizationEvaluationAdapter;
import de.uni_rostock.beacon.fragments.Fragment;

/**
 * @author Johann Bauer
 *         created 12.03.2016
 */
public class BeaconEvaluationListFragment extends Fragment {
    private View v;
    public static BeaconEvaluationDetailFragment detailFragment = new BeaconEvaluationDetailFragment();
    public static EvaluationFragment addFragment = new EvaluationFragment();
    private Gson gson = new GsonBuilder().create();
    private List<LocalizationEvaluationAdapter.EvalutionRun> runs;
    private RecyclerView rv;
    private ProgressBar pb;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        File mFolder = new File(Logger.path);
        Toast.makeText(MyApplication.getActivity(), mFolder.listFiles().length + " Dateien werden analysiert...", Toast.LENGTH_LONG).show();
        v = inflater.inflate(R.layout.fragment_beacon_evaluation_list, container, false);
        pb = (ProgressBar) v.findViewById(R.id.progressBar);
        new GetEvaluationRunsTask().execute();
        pb.setVisibility(View.VISIBLE);
        rv = (RecyclerView)v.findViewById(R.id.rv);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        rv.setLayoutManager(llm);
        if (runs != null) {
            LocalizationEvaluationAdapter adapter = new LocalizationEvaluationAdapter(runs);
            rv.setAdapter(adapter);
        }
        FloatingActionButton fab = (FloatingActionButton) v.findViewById(R.id.fab);
        fab.attachToRecyclerView(rv);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyApplication.getActivity().showFragment(addFragment);
            }
        });
        detailFragment.setParent(this);
        addFragment.setParent(this);
        return v;
    }

    @Override
    public void onDestroyView() {
        rv = null;
        pb = null;
        super.onDestroyView();
    }

    @Override
    public String getName() {
        return "Auswertung für Beacons";
    }

    @Override
    public String getCategory() {
        return "Evaluation";
    }

    @Override
    public Integer getMenuIndex() {
        return 17+4;
    }

    private class GetEvaluationRunsTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            getEvalutionRuns();
            return null;
        }
    }

    /**
     * Liefer eine Liste von allen Beobachtungen mit Genauigkeit.
     */
    public void getEvalutionRuns() {
        File mFolder = new File(Logger.path);
        if (!mFolder.exists()) {
            Toast.makeText(MyApplication.getActivity(), "Es sind noch keine Beacon-Logs gespeichert", Toast.LENGTH_LONG).show();
            return;
        }
        final List<LocalizationEvaluationAdapter.EvalutionRun> runs = new LinkedList<>();
        File files[] = mFolder.listFiles();
        Arrays.sort(files);
        final int total = files.length;
        int i = 0;

        for (File f: files) {
            i++;
            final int currentI = i;
            MyApplication.getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (pb == null)
                        return;
                    pb.setMax(total);
                    pb.setProgress(currentI);
                }
            });
            if (!f.getName().startsWith("beacon_log_") || !f.getName().endsWith(".json"))
                continue;
            Long date;
            Logger.MetaDataHolder mdh = null;
            File metaFile = new File(f.getAbsolutePath() + ".meta");
            if (metaFile.exists()) {
                try {
                    JsonReader reader = new JsonReader(new FileReader(metaFile.getAbsolutePath()));
                    try {
                        mdh = gson.fromJson(reader, Logger.MetaDataHolder.class);
                    }
                    catch (JsonSyntaxException e) {
                        // Fehlerhafte Daten einfach ignorieren, dann gibt es für diesen Lauf keine Ortsgenauigkeit

                    }

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
            List<Logger.Observation> observations = new LinkedList<>();
            String name = f.getName().replace("beacon_log_", "").replace(".json", "");
            date = Long.valueOf(name);
            try {
                JsonReader reader = new JsonReader(new FileReader(f.getAbsolutePath()));
                Type listType = new TypeToken<List<Logger.Observation>>() {}.getType();
                try {
                    observations = gson.fromJson(reader, listType);
                }
                catch (JsonSyntaxException e) {
                    // Fehlerhafte Daten einfach ignorieren
                    continue;
                }

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            if (observations == null)
                continue;
            LocalizationEvaluationAdapter.EvalutionRun run = new LocalizationEvaluationAdapter.EvalutionRun(date, observations);
            run.setMdh(mdh);
            runs.add(run);

        }
        this.runs = runs;

        if (rv != null) {
            MyApplication.getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    rv.setAdapter(new LocalizationEvaluationAdapter(runs));
                    pb.setVisibility(View.INVISIBLE);
                }
            });
        }

    }

}
