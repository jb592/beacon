package de.uni_rostock.beacon.bluetooth.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Date;
import java.util.List;

import de.uni_rostock.beacon.Location;
import de.uni_rostock.beacon.MyApplication;
import de.uni_rostock.beacon.Position;
import de.uni_rostock.beacon.R;
import de.uni_rostock.beacon.bluetooth.Beacon;
import de.uni_rostock.beacon.bluetooth.Logger;
import de.uni_rostock.beacon.bluetooth.fragments.BeaconEvaluationListFragment;

public class LocalizationEvaluationAdapter extends RecyclerView.Adapter {
    private List<EvalutionRun> runs;
    public LocalizationEvaluationAdapter(List<EvalutionRun> runs) {
        this.runs = runs;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_beacon_evaluations, parent, false);
        BeaconEvaluationViewHolder pvh = new BeaconEvaluationViewHolder(v);
        return pvh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        EvalutionRun run = runs.get(position);
        Long date = run.getDate();
        Date d = new Date(date);
        ((BeaconEvaluationViewHolder) holder).name.setText(d.toString());
        ((BeaconEvaluationViewHolder) holder).count.setText(run.getObservations().size() + " Messungen");
        ((BeaconEvaluationViewHolder) holder).beaconAccuracy.setText("korrekter Beacon: " + run.getBeaconAccuracy()*100 + "%");
        ((BeaconEvaluationViewHolder) holder).locationAccuracy.setText("korrekter Ort: " + run.getLocationAccuracy()*100 + "%");

        ((BeaconEvaluationViewHolder)holder).item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyApplication.getActivity().showFragment(BeaconEvaluationListFragment.detailFragment);
                BeaconEvaluationListFragment.detailFragment.setRun(runs.get(position));

            }
        });
    }

    @Override
    public int getItemCount() {
        return runs.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public static class BeaconEvaluationViewHolder extends RecyclerView.ViewHolder {
        TextView name;
        TextView count;
        TextView beaconAccuracy;
        TextView locationAccuracy;
        View item;

        BeaconEvaluationViewHolder(View itemView) {
            super(itemView);
            item = itemView;
            name = (TextView) itemView.findViewById(R.id.name);
            beaconAccuracy = (TextView) itemView.findViewById(R.id.beaconAccuracy);
            locationAccuracy = (TextView) itemView.findViewById(R.id.locationAccuracy);
            count = (TextView) itemView.findViewById(R.id.count);
        }
    }

    public static class EvalutionRun {
        private Logger.MetaDataHolder mdh;
        private Long date;
        private List<Logger.Observation> observations;
        private float beaconAccuracy = -1;
        private float locationAccuracy = -1;

        public Logger.MetaDataHolder getMdh() {
            return mdh;
        }

        public void setMdh(Logger.MetaDataHolder mdh) {
            this.mdh = mdh;
        }

        public Long getDate() {
            return date;
        }

        public List<Logger.Observation> getObservations() {
            return observations;
        }

        public float getLocationAccuracy() {
            if (mdh == null)
                return 0.0f;
            if (locationAccuracy == -1) {
                locationAccuracy = getAccuracy(false);
            }
            return locationAccuracy;
        }

        private float getAccuracy(boolean forBeacon) {
            // Berechne Genauigkeit "on-demand"
            float accuracy = 0;
            for (Logger.Observation o: observations) {
                int maxRssi = -Integer.MAX_VALUE;
                Beacon c = null;
                for (Beacon b: o.what) {
                    if (b.getRssi() > maxRssi) {
                        maxRssi = b.getRssi();
                        c = b;
                    }
                }
                if(o.closest == null) continue;
                if (forBeacon && c != null && o.closest.getAddress().equals(c.getAddress())) {
                    accuracy++;
                }
                if (!forBeacon && c != null) {
                    Position measuredPosition;
                    Position realPosition;
                    measuredPosition = c.getPosition();
                    realPosition = o.closest.getPosition();
                    if (measuredPosition == null || realPosition == null)
                        continue;
                    boolean same = true;
                    for (Location l: mdh.locations) {
                        if (l.getMapName() != null && l.getMapName().equals(measuredPosition.getMapName()))
                            if (l.getRegion().contains(measuredPosition.getX(), measuredPosition.getY()) != l.getRegion().contains(realPosition.getX(), realPosition.getY()) || !measuredPosition.getMapName().equals(realPosition.getMapName()))
                                same = false;
                    }
                    if (same)
                        accuracy++;
                }

            }
            accuracy /= observations.size();
            return accuracy;
        }

        public float getBeaconAccuracy() {
            if (beaconAccuracy == -1) {
                beaconAccuracy = getAccuracy(true);
            }
            return beaconAccuracy;
        }

        public EvalutionRun(Long date, List<Logger.Observation> observations) {
            this.date = date;
            this.observations = observations;
        }
    }
}
