package de.uni_rostock.beacon.bluetooth.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.melnykov.fab.FloatingActionButton;

import de.uni_rostock.beacon.MyApplication;
import de.uni_rostock.beacon.R;
import de.uni_rostock.beacon.bluetooth.BeaconManager;
import de.uni_rostock.beacon.bluetooth.adapters.BeaconAdapter;
import de.uni_rostock.beacon.fragments.Fragment;

/**
 * @author Johann Bauer
 *         created 17.01.2016
 */
public class BeaconListFragment extends Fragment {
    private View v;
    public static BeaconDetailFragment detailFragment = new BeaconDetailFragment();
    public static BeaconSearchFragment searchFragment = new BeaconSearchFragment();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_beacon_list, container, false);
        RecyclerView rv = (RecyclerView)v.findViewById(R.id.rv);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        rv.setLayoutManager(llm);
        BeaconAdapter adapter = new BeaconAdapter(BeaconManager.getAddedBeacons());
        rv.setAdapter(adapter);
        FloatingActionButton fab = (FloatingActionButton) v.findViewById(R.id.fab);
        fab.attachToRecyclerView(rv);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyApplication.getActivity().showFragment(searchFragment);
            }
        });
        detailFragment.setParent(this);
        searchFragment.setParent(this);
        return v;
    }

    @Override
    public String getName() {
        return "Beacons";
    }

    @Override
    public Integer getMenuIndex() {
        return 0;
    }

    @Override
    public String getCategory() {
        return "Verwalten";
    }
}
