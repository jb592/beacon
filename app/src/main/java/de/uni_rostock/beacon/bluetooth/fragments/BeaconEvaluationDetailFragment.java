package de.uni_rostock.beacon.bluetooth.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import de.uni_rostock.beacon.Location;
import de.uni_rostock.beacon.MyApplication;
import de.uni_rostock.beacon.R;
import de.uni_rostock.beacon.bluetooth.Beacon;
import de.uni_rostock.beacon.bluetooth.Logger;
import de.uni_rostock.beacon.bluetooth.adapters.BeaconEvaluationDetailAdapter;
import de.uni_rostock.beacon.bluetooth.adapters.LocalizationEvaluationAdapter;
import de.uni_rostock.beacon.fragments.Fragment;

/**
 * @author Johann Bauer
 *         created 12.03.2016
 */
public class BeaconEvaluationDetailFragment extends Fragment {
    private Fragment parent;
    private View v;
    private LocalizationEvaluationAdapter.EvalutionRun run;
    BeaconEvaluationDetailAdapter adapter;
    private RecyclerView rv;

    public void setRun(LocalizationEvaluationAdapter.EvalutionRun run) {
        this.run = run;
        if (rv == null ||v == null)
            return;
        adapter = new BeaconEvaluationDetailAdapter(calculate());
        rv.setAdapter(adapter);

        SimpleDateFormat sdf = new SimpleDateFormat("E, dd MMM yyyy HH:mm:ss z");
        String date = sdf.format(run.getDate());

        ((TextView)v.findViewById(R.id.name)).setText("Testlauf vom " + date);
        ((TextView)v.findViewById(R.id.accuracy)).setText(run.getBeaconAccuracy() * 100 + "%");
        ((TextView)v.findViewById(R.id.observationCount)).setText(run.getObservations().size() + " Messpunkte.");

    }

    /**
     * Setzt das Fragment, zu dem zurückgekehrt werden soll, wenn dieses Fragment beendet.
     * @param parent Fragment
     */
    public void setParent(Fragment parent) {
        this.parent = parent;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_beacon_evaluation_detail, container, false);
        Spinner modeSpinner = (Spinner) v.findViewById(R.id.modeSpinner);
        ArrayAdapter<String> sAdapter = new ArrayAdapter<>(
                MyApplication.getActivity(), android.R.layout.simple_spinner_dropdown_item, new String[] {"Korrekter Ort", "Korrekter Beacon"});
        modeSpinner.setAdapter(sAdapter);
        modeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                adapter.setShowBeaconAccuracy((position == 1));
                if (position != 0)
                    ((TextView)v.findViewById(R.id.accuracy)).setText(run.getBeaconAccuracy() * 100 + "%");
                else
                    ((TextView)v.findViewById(R.id.accuracy)).setText(run.getLocationAccuracy() * 100 + "%");
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        rv = (RecyclerView)v.findViewById(R.id.rv);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        rv.setLayoutManager(llm);
        if (run != null) {
            setRun(run);
        }
        return v;
    }

    /**
     * Berechnet die Daten, die angezeigt werden sollen. Dabei wird eine Rangliste der Beacons
     * nach Anzahl der Beobachtungen angelegt und dann für alle Beacons in dieser Rangliste
     * die Genauigkeit ohne diesen Beacon berechnet.
     * @return
     */
    private LinkedList<AlteredTestRun> calculate() {
        // Ranking erstellen...
        HashMap<String, Pair<Beacon, Integer>> storage = new HashMap<>();
        for (Logger.Observation i: run.getObservations()) {
            Beacon c = getClosestForObservation(i);
            if (!storage.containsKey(c.getAddress())) {
                storage.put(c.getAddress(), new Pair<>(c, 0));
            }
            storage.put(c.getAddress(), increase(storage.get(c.getAddress())));
        }
        LinkedList<Beacon> ranking = new LinkedList<>();
        while (storage.size() > 0) {
            int min = Integer.MAX_VALUE;
            String key = null;
            Beacon worst = null;
            for (Map.Entry<String, Pair<Beacon, Integer>> e: storage.entrySet()) {
                if (e.getValue().second < min) {
                    min = e.getValue().second;
                    worst = e.getValue().first;
                    key = e.getKey();
                }
            }
            ranking.add(worst);
            storage.remove(key);
        }

        LinkedList<AlteredTestRun> stats = new LinkedList<>();
        LinkedList<Beacon> removed = new LinkedList<>();
        for (Beacon b: ranking) {
            removed.add(b);
            stats.add(new AlteredTestRun(b, getAccuracyWithoutBeacons(removed, true), getAccuracyWithoutBeacons(removed, false)));
        }
        return stats;
    }

    /**
     * Berechnet die Genauigkeit der Messung ohne die angegebenen Beacons.
     * @param removed Beacons, die bei der Genauigkeit ignoriert werden sollen.
     * @return Genauigkeit im Intervall [0,1]
     */
    private float getAccuracyWithoutBeacons(LinkedList<Beacon> removed, boolean forBeacons) {
        float accuracy = 0;
        for (Logger.Observation o: run.getObservations()) {
            Beacon c = getClosestForObservation(o);
            boolean found = false;
            for (Beacon i: removed) {
                if (i.getAddress().equals(c.getAddress()))
                    found = true;
            }
            if (found)
                continue;
            if (forBeacons && o.closest != null && o.closest.getAddress().equals(c.getAddress()))
                accuracy++;
            if (!forBeacons && o.closest != null && o.closest.getPosition() != null && c.getPosition() != null) {
                boolean same = true;
                for (Location l: run.getMdh().locations) {
                    if (l.getMapName() != null && l.getMapName().equals(c.getPosition().getMapName()))
                        if (l.getRegion().contains(o.closest.getPosition().getX(), o.closest.getPosition().getY()) !=
                                l.getRegion().contains(c.getPosition().getX(), c.getPosition().getY()) || !c.getPosition().getMapName().equals(o.closest.getPosition().getMapName()))
                            same = false;
                }
                if (same)
                    accuracy++;
            }
        }
        accuracy /= run.getObservations().size();
        return accuracy;
    }

    public static class AlteredTestRun {
        private Beacon removed;
        private float beaconAccuracy;
        private float locationAccuracy;

        public Beacon getRemoved() {
            return removed;
        }

        public float getBeaconAccuracy() {
            return beaconAccuracy;
        }

        public float getLocationAccuracy() {
            return locationAccuracy;
        }

        public AlteredTestRun(Beacon removed, float beaconAccuracy, float locationAccuracy) {
            this.removed = removed;
            this.beaconAccuracy = beaconAccuracy;
            this.locationAccuracy = locationAccuracy;
        }
    }


    private Pair<Beacon, Integer> increase(Pair<Beacon, Integer> i) {
        return new Pair<>(i.first, i.second+1);
    }

    /**
     * Gibt den Beacon zurück, der in der Beobachtung die größte Empfangsstärke hatte.
     * @param o Beobachtung
     * @return Beacon
     */
    private Beacon getClosestForObservation(Logger.Observation o) {
        Integer maxRssi = -Integer.MAX_VALUE;
        Beacon closest = null;
        for (Beacon b: o.what) {
            if (b.getRssi() > maxRssi) {
                maxRssi = b.getRssi();
                closest = b;
            }
        }
        return closest;
    }

    @Override
    public boolean onBackButtonPress() {
        if (parent != null) {
            MyApplication.getActivity().showFragment(parent);
            return true;
        }
        return false;
    }
}
