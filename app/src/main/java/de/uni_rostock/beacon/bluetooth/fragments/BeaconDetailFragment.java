package de.uni_rostock.beacon.bluetooth.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.malinskiy.materialicons.IconDrawable;
import com.malinskiy.materialicons.Iconify;

import java.util.List;

import de.uni_rostock.beacon.Location;
import de.uni_rostock.beacon.MapData;
import de.uni_rostock.beacon.MapDataManager;
import de.uni_rostock.beacon.MyApplication;
import de.uni_rostock.beacon.Position;
import de.uni_rostock.beacon.R;
import de.uni_rostock.beacon.bluetooth.Beacon;
import de.uni_rostock.beacon.bluetooth.LocationManager;
import de.uni_rostock.beacon.fragments.Fragment;
import de.uni_rostock.beacon.views.DrawingView;


public class BeaconDetailFragment extends Fragment {

    private View v;
    private Beacon b;
    private Fragment parent;
    private EditText description;
    private final List<MapData> maps = new MapDataManager().getMapDatas();
    private Spinner spinner;
    private ProgressBar progressBar;
    private Position selectedPosition;
    private MapData selectedMapData;
    private DrawingView mapImage;
    EditText _description;

    /**
     * Setzt das Fragment, zu dem zurückgekehrt werden soll, wenn dieses Fragment beendet.
     * @param parent Fragment
     */
    public void setParent(Fragment parent) {
        this.parent = parent;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_beacon_detail, container, false);
        onAfterCreateView(v);
        ((EditText)v.findViewById(R.id.description)).setText("");
        selectedMapData = null;
        selectedPosition = null;
        mapImage = (DrawingView) v.findViewById(R.id.map_image);
        mapImage.setMode(DrawingView.Mode.DRAW_POINT);
        if (b != null)
        {
            setBeacon(b);
        }

        _description = description;
        Toast.makeText(getActivity(), "Benutzen Sie den Stift, um den Beacon auf der Karte zu platzieren", Toast.LENGTH_LONG).show();
        return v;
    }

    @Override
    public void onStart() {
        super.onStart();
        if (b != null)
            description.setText(b.getDescription());
        spinner = (Spinner) v.findViewById(R.id.map);
        int i = 0;
        for (MapData d: maps) {
            if (b.getPosition() != null && d.getName().equals(b.getPosition().getMapName()))
                break;
            i++;
        }
        if (i < maps.size())
            spinner.setSelection(i);
    }

    /**
     * Setzt den Beacon, zu dem Details angezeigt werden sollen.
     * @param b Beacon
     */
    public void setBeacon(Beacon b) {
        this.b = b;
        if (v != null) {
            selectedPosition = b.getPosition();
            description = (EditText) v.findViewById(R.id.description);
            TextView name = (TextView) v.findViewById(R.id.name);
            TextView address = (TextView) v.findViewById(R.id.address);
            mapImage = (DrawingView) v.findViewById(R.id.map_image);
            final TextView errorMessage = (TextView) v.findViewById(R.id.error_message);
            description.setText(b.getDescription());
            name.setText(b.getName());
            address.setText(b.getAddress());
            spinner = (Spinner) v.findViewById(R.id.map);
            progressBar = (ProgressBar) v.findViewById(R.id.progressBar);
             // Create an ArrayAdapter using the string array and a default spinner layout
            ArrayAdapter<MapData> adapter = new ArrayAdapter<>(
                    MyApplication.getActivity(), android.R.layout.simple_spinner_dropdown_item, maps);
            // Apply the adapter to the spinner
            spinner.setAdapter(adapter);
            if (maps.size() > 0)
                spinner.setSelection(0);
            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    progressBar.setVisibility(View.VISIBLE);
                    mapImage.setVisibility(View.INVISIBLE);
                    selectedMapData = maps.get(position);
                    final List<Location> locations = LocationManager.findLocationsByMapName(selectedMapData.getName());
                    errorMessage.setVisibility(View.INVISIBLE);
                    mapImage.setMapData(selectedMapData);
                    for (Location l: locations) {
                        mapImage.addRegionToDraw(l.getRegion(), l.getName());
                    }
                    mapImage.setVisibility(View.VISIBLE);
                    progressBar.setVisibility(View.INVISIBLE);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                    errorMessage.setVisibility(View.VISIBLE);
                }
            });
            int i = 0;
            for (MapData d: maps) {
                if (b.getPosition() != null && d.getName().equals(b.getPosition().getMapName()))
                    break;
                i++;
            }
            if (i < maps.size())
                spinner.setSelection(i);
            mapImage.setPositionToDraw(selectedPosition);
        }
    }


    @Override
    public boolean onBackButtonPress() {
        if (parent != null) {
            MyApplication.getActivity().showFragment(parent);
            return true;
        }
        return false;
    }

    @Override
    public String getName() {
        if (b != null)
            return "Beacon " + b.getName();
        return "Beacon-Details";
    }


    @Override
    public void onInsertMenu(Menu m) {
        MenuItem i = m.add("Speichern");
        i.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                String description = _description.getText().toString();
                b.setDescription(description);
                selectedPosition = mapImage.getPositionToDraw();
                if (selectedPosition != null) {
                    selectedPosition.setMapName(maps.get(spinner.getSelectedItemPosition()).getName());
                    b.setPosition(selectedPosition);
                }
                b.save();
                getActivity().onBackPressed();
                return true;
            }
        });
        i.setIcon(new IconDrawable(MyApplication.getActivity(), Iconify.IconValue.zmdi_save)
                .colorRes(R.color.md_white_1000)
                .actionBarSize());
        i.setShowAsAction(MenuItem.SHOW_AS_ACTION_WITH_TEXT | MenuItem.SHOW_AS_ACTION_ALWAYS);
    }
}
