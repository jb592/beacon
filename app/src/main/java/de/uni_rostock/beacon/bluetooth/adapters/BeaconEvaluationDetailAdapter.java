package de.uni_rostock.beacon.bluetooth.adapters;

import android.animation.ObjectAnimator;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.LinkedList;
import java.util.List;

import de.uni_rostock.beacon.R;
import de.uni_rostock.beacon.bluetooth.fragments.BeaconEvaluationDetailFragment;

public class BeaconEvaluationDetailAdapter extends RecyclerView.Adapter {
    private List<BeaconEvaluationDetailFragment.AlteredTestRun> runs;
    public BeaconEvaluationDetailAdapter(LinkedList<BeaconEvaluationDetailFragment.AlteredTestRun> runs) {
        this.runs = runs;
    }

    private boolean showBeaconAccuracy = false;
    public void setShowBeaconAccuracy(boolean showBeaconAccuracy) {
        this.showBeaconAccuracy = showBeaconAccuracy;
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_beacon_evaluation_run, parent, false);
        BeaconEvaluationViewHolder pvh = new BeaconEvaluationViewHolder(v);
        return pvh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        ((BeaconEvaluationViewHolder) holder).count.setText((position+1) + ((position > 0) ? " entfernte Beacons.": " entfernter Beacon."));
        String explaination = "Durch ";
        if (position > 0) {
            explaination += "zusätzliches ";
        }
        explaination += "Entfernen des Beacons " + runs.get(position).getRemoved().getName();
        explaination += " sinkt die Genauigkeit auf:";
        ((BeaconEvaluationViewHolder) holder).explaination.setText(explaination);
        float accuracy;
        if (showBeaconAccuracy)
            accuracy = runs.get(position).getBeaconAccuracy() * 100;
        else
            accuracy = runs.get(position).getLocationAccuracy() * 100;
        ((BeaconEvaluationViewHolder) holder).accuracy.setText(accuracy + "%");
        ObjectAnimator animation = ObjectAnimator.ofInt(((BeaconEvaluationViewHolder) holder).accuracyProgress, "progress", (int) (accuracy));
        animation.setDuration(750); // 0.75 seconds
        animation.setInterpolator(new DecelerateInterpolator());
        animation.start();
        ((BeaconEvaluationViewHolder) holder).id.setText(runs.get(position).getRemoved().getAddress());
    }

    @Override
    public int getItemCount() {
        return runs.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public static class BeaconEvaluationViewHolder extends RecyclerView.ViewHolder {
        TextView explaination;
        TextView count;
        TextView accuracy;
        TextView id;
        ProgressBar accuracyProgress;
        View item;

        BeaconEvaluationViewHolder(View itemView) {
            super(itemView);
            item = itemView;
            accuracy = (TextView) itemView.findViewById(R.id.accuracy);
            count = (TextView) itemView.findViewById(R.id.count);
            explaination = (TextView) itemView.findViewById(R.id.explaination);
            accuracyProgress = (ProgressBar) itemView.findViewById(R.id.accuracyProgress);
            id = (TextView) itemView.findViewById(R.id.id);
        }
    }

}
