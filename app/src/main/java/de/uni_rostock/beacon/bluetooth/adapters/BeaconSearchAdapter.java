package de.uni_rostock.beacon.bluetooth.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import de.uni_rostock.beacon.MyApplication;
import de.uni_rostock.beacon.R;
import de.uni_rostock.beacon.bluetooth.Beacon;
import de.uni_rostock.beacon.bluetooth.fragments.BeaconSearchFragment;

public class BeaconSearchAdapter extends RecyclerView.Adapter {
    private List<Beacon> beacons;
    public BeaconSearchAdapter(List<Beacon> beacons) {
        this.beacons = beacons;
    }

    public List<Beacon> getBeacons() {
        return beacons;
    }

    public void addBeacon(Beacon b) {
        beacons.add(b);
        MyApplication.getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                notifyItemInserted(beacons.size() - 1);
            }
        });

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_unsaved_beacons, parent, false);
        PlacesViewHolder pvh = new PlacesViewHolder(v);
        return pvh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        ((PlacesViewHolder)holder).name.setText(beacons.get(position).getName());
        ((PlacesViewHolder)holder).rssi.setText(beacons.get(position).getRssi() + "dBm");
        ((PlacesViewHolder)holder).additionalID.setText(beacons.get(position).getAdditionalID());
        ((PlacesViewHolder)holder).address.setText(beacons.get(position).getAddress());
        ((PlacesViewHolder)holder).item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyApplication.getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        MyApplication.getActivity().showFragment(BeaconSearchFragment.detailFragment);
                        BeaconSearchFragment.detailFragment.setBeacon(beacons.get(position));
                    }
                });
            }
        });
    }

    @Override
    public int getItemCount() {
        return beacons.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public static class PlacesViewHolder extends RecyclerView.ViewHolder {
        TextView name;
        TextView address;
        TextView rssi;
        View item;
        TextView additionalID;

        PlacesViewHolder(View itemView) {
            super(itemView);
            item = itemView;
            name = (TextView) itemView.findViewById(R.id.name);
            address = (TextView) itemView.findViewById(R.id.address);
            rssi = (TextView) itemView.findViewById(R.id.rssi);
            additionalID = (TextView) itemView.findViewById(R.id.additional_id);
        }
    }
}
