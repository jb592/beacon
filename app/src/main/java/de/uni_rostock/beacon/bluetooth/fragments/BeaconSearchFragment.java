package de.uni_rostock.beacon.bluetooth.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;
import java.util.Observable;
import java.util.Observer;

import de.uni_rostock.beacon.MyApplication;
import de.uni_rostock.beacon.R;
import de.uni_rostock.beacon.bluetooth.Beacon;
import de.uni_rostock.beacon.bluetooth.BeaconManager;
import de.uni_rostock.beacon.bluetooth.BluetoothLocalization;
import de.uni_rostock.beacon.bluetooth.adapters.BeaconSearchAdapter;
import de.uni_rostock.beacon.fragments.Fragment;

/**
 * @author Johann Bauer
 *         created 21.02.2016
 */
public class BeaconSearchFragment extends Fragment implements Observer{
    private View v;
    private BeaconSearchAdapter adapter;
    public static BeaconDetailFragment detailFragment = new BeaconDetailFragment();
    private Fragment parent;

    public void setParent(Fragment parent) {
        this.parent = parent;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_beacon_search_list, container, false);
        RecyclerView rv = (RecyclerView)v.findViewById(R.id.rv);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        rv.setLayoutManager(llm);
        adapter = new BeaconSearchAdapter(BeaconManager.getActiveUnknownBeacons());
        rv.setAdapter(adapter);
        detailFragment.setParent(parent);
        ((BluetoothLocalization)MyApplication.getLocalization()).addObserver(this);
        return v;
    }

    @Override
    public void onDestroyView() {
        ((BluetoothLocalization)MyApplication.getLocalization()).deleteObserver(this);
        super.onDestroyView();
    }

    @Override
    public String getName() {
        return "Beacons in Reichweite";
    }

    @Override
    public void update(Observable observable, Object data) {
        if (adapter == null)
            return;

        // RSSI Updaten
        if (data instanceof Beacon) {
            final int i = adapter.getBeacons().indexOf(data);
            if (i != -1)
                MyApplication.getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        adapter.notifyItemChanged(i);
                    }
                });

        }
        // Beacon hinzufügen, der nicht in der Liste ist
        List<Beacon> in = adapter.getBeacons();
        List<Beacon> out = BeaconManager.getActiveUnknownBeacons();
        for (Beacon b: out) {
            if (!in.contains(b)) {
                adapter.addBeacon(b);
            }
        }

    }

    @Override
    public boolean onBackButtonPress() {
        if (parent != null) {
            MyApplication.getActivity().showFragment(parent);
            return true;
        }
        return false;
    }
}
