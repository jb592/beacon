package de.uni_rostock.beacon.bluetooth;


import com.orm.SugarRecord;

import de.uni_rostock.beacon.Position;

/**
 * Diese Klasse beschreibt einen Beacon.
 *
 * @author Johann Bauer
 * created 2016-01-17
 */

public class Beacon extends SugarRecord {
    private String name;
    private Integer rssi;
    private String address;
    private Position position;
    private String description = "Unbenannt";
    private String uuid;
    private String additionalID;

    /**
     * Gibt die UUID vom Beacon zurück. Diese wird aus dem iBeacon/Eddystone-Paket extrahiert.
     * @see BeaconManager
     * @return UUID oder null, falls keine UUID empfangen wurde
     */
    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    /**
     * Gibt die zusätzlichen IDs im lesbaren Format zurück. Bei Eddystone wäre es die Location ID,
     * bei iBeacon die Major und Minor ID im Format "Major XX Minor XX".
     * @see BeaconManager
     * @return ID
     */
    public String getAdditionalID() {
        return additionalID;
    }

    public void setAdditionalID(String additionalID) {
        this.additionalID = additionalID;
    }

    /**
     * Gibt die vom Nutzer vergebene Beschreibung des Beacons zurück.
     * @return Beschreibung
     */
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    // Wenn ein Beacon so lange nicht sendet, ist er wohl außer Reichweite
    private static final int INACTIVE_TIMEOUT = 2700;

    /**
     * Gibt einen Zeitstempel (Unix-Timestamp in ms) zurück, bis zu dem der Beacon als aktiv
     * angesehen wird.
     * @return Zeitstempel
     */
    public Long getValidUntil() {
        return validUntil;
    }

    public void ping() {
        this.validUntil = System.currentTimeMillis() + INACTIVE_TIMEOUT;
    }

    /**
     * Gibt den Radio Signal Strength Indicator (RSSI) zurück.
     * @return RSSI
     */
    public Integer getRssi() {
        return rssi;
    }

    public void setRssi(Integer rssi) {
        this.rssi = rssi;
    }

    /**
     * Gibt die Adresse des Beacons zurück. Das ist normalerweise eine Adresse aus dem iBeacon-
     * bzw. Eddystone-Paket. Wird vom Beacon allerdings kein entsprechendes Paket gesendet,
     * wird hier nur die MAC-Adresse zurückgegeben.
     * @return Adresse
     */
    public String getAddress() {
        if (getUuid() != null) {
            // Da die MAC-Adressen sich bei manchen Beacons verändern, ist es besser,
            // sich auf die Proximity UUID zu verlassen, wenn sie existiert
            return getUuid();
        }
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    private transient Long validUntil;

    /**
     * Gibt den Namen zurück, den der Beacon über Bluetooth ausstrahlt.
     * @return Bluetooth-Name
     */
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Beacon(String name, Integer rssi, String address) {
        this.name = name;
        this.rssi = rssi;
        this.address = address;
    }

    /**
     * Gibt die Position des Beacons zurück.
     * @return Position des Beacons oder null.
     */
    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        position.save();
        this.position = position;
    }

    public Beacon() {

    }
}
