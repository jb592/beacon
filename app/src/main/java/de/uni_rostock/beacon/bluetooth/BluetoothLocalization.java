package de.uni_rostock.beacon.bluetooth;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.neovisionaries.bluetooth.ble.advertising.ADPayloadParser;
import com.neovisionaries.bluetooth.ble.advertising.ADStructure;
import com.neovisionaries.bluetooth.ble.advertising.EddystoneUID;
import com.neovisionaries.bluetooth.ble.advertising.IBeacon;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import de.uni_rostock.beacon.ILocalization;
import de.uni_rostock.beacon.MyApplication;
import de.uni_rostock.beacon.Position;
import de.uni_rostock.beacon.bluetooth.fragments.BeaconEvaluationListFragment;
import de.uni_rostock.beacon.bluetooth.fragments.BeaconListFragment;
import de.uni_rostock.beacon.bluetooth.fragments.EvaluationFragment;
import de.uni_rostock.beacon.fragments.Fragment;

/**
 * @author Johann Bauer
 *         created 08.02.2016
 */
public class BluetoothLocalization extends Observable implements ILocalization {
    private static BluetoothAdapter mBluetoothAdapter;
    private Callback callback = new Callback(this);


    @Override
    public void start() {
        // Initializes Bluetooth adapter.
        final BluetoothManager bluetoothManager =
                (BluetoothManager) MyApplication.application.getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = bluetoothManager.getAdapter();
        if (!mBluetoothAdapter.isEnabled()) {
            // asynchron!
            mBluetoothAdapter.enable();
        }
        // Die neuen Methoden können leider nicht verwendet werden, da das Tablet noch mit Android 4 läuft.
        if (!mBluetoothAdapter.startLeScan(callback)) {
            Toast.makeText(MyApplication.getActivity(), "BluetoothLE-Scan konnte nicht gestartet werden. Bitte starten Sie das Gerät neu.", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public Position getPosition() {
        Beacon c = BeaconManager.getClosestPositionedBeacon();
        Log.d("Localization", "closest Beacon is " + (c != null ? c.getName() : "null"));
        if (c == null)
            return null;
        Log.d("Localization", "closest Beacon has position " + (c.getPosition() != null ? c.getPosition() : "null"));
        return c.getPosition();
    }

    @Override
    public void addOnPositionChangeListener(Observer o) {
        addObserver(o);
    }

    @Override
    public void removeOnPositionChangeListener(Observer o) {
        deleteObserver(o);
    }


    private class Callback implements BluetoothAdapter.LeScanCallback {
        private final BluetoothLocalization parent;
        public Callback(BluetoothLocalization parent) {
            this.parent = parent;
        }

        @Override
        public void onLeScan(BluetoothDevice bluetoothDevice, int rssi, byte[] bytes) {
            try {
                final String TAG = "Localization";

                String uuid = null;
                String additionalID = null;


                // Parse the payload of the advertisement packet
                // as a list of AD structures.
                List<ADStructure> structures =
                        ADPayloadParser.getInstance().parse(bytes);

                for (ADStructure structure : structures)
                {
                    // If the AD structure represents Eddystone UID.
                    if (structure instanceof EddystoneUID)
                    {
                        EddystoneUID es = (EddystoneUID)structure;
                        uuid = es.getBeaconIdAsString();
                        additionalID = "NS: " + es.getNamespaceIdAsString() +  " - " + es.getInstanceIdAsString();
                    }
                    // If the AD structure represents iBeacon.
                    else if (structure instanceof IBeacon)
                    {
                        // iBeacon advert
                        IBeacon es = (IBeacon)structure;
                        uuid = es.getUUID().toString() + "-" + es.getMajor() + "-" + es.getMinor();
                        additionalID = "Major " + es.getMajor() + " Minor " + es.getMinor();
                    }
                }
                Beacon current = BeaconManager.getBeaconByAddress(uuid == null ?bluetoothDevice.getAddress(): uuid);
                if (additionalID != null) current.setAdditionalID(additionalID);
                current.setUuid(uuid);
                current.setName(bluetoothDevice.getName());
                current.setRssi(rssi);
                current.setAddress(bluetoothDevice.getAddress());
                current.ping();

                synchronized (parent) {
                    parent.setChanged();
                    parent.notifyObservers(current);
                }
            }
            catch (Exception e) {
                e.printStackTrace();
            }


        }
    }

    @Override
    public void stop() {
        mBluetoothAdapter.stopLeScan(callback);
    }

    @Override
    public List<Fragment> getAdditionalFragments() {
        List<Fragment> x = new ArrayList<>();
        x.add(new BeaconListFragment());
        x.add(new EvaluationFragment());
        x.add(new BeaconEvaluationListFragment());
        return x;
    }


    private Logger logger = null;
    @Override
    public void onSetRealPosition(Position real) {
        // Logger werden nur auf Wunsch erzeugt, da sonst leere Dateien angelegt würden
        if (logger == null) {
            logger = new Logger();
        }
        Logger.Observation o = new Logger.Observation();
        o.what = BeaconManager.getActiveBeacons();
        List<Beacon> positioned = BeaconManager.getPlacedBeacons();
        if (positioned.size() == 0)
        {
            logger = null;
            return;
        }
        double distance = getDistance(real, positioned.get(0).getPosition());
        for (Beacon b: positioned) {
            if (getDistance(real, b.getPosition()) < distance) {
                distance = getDistance(real, b.getPosition());
                // Das ist unser Beacon!
                o.closest = b;
            }
        }
        logger.log(o);
    }

    /**
     * Berechnet die Distanz zwischen den Punkten p und q.
     * @param p Punkt
     * @param q Punkt
     * @return Distanz zwischen p und q
     */
    private double getDistance(Position p, Position q)
    {
        return Math.sqrt(Math.pow((q.getX() - p.getX()), 2) + Math.pow((q.getY() - p.getY()), 2));
    }

    @Override
    public void onObservationEnd() {
        if(logger != null)
        {
            logger.commit();
            logger = null;
        }
    }
}
