package de.uni_rostock.beacon.bluetooth;

import android.graphics.Region;

import java.util.LinkedList;
import java.util.List;

import de.uni_rostock.beacon.Location;
import de.uni_rostock.beacon.Position;

/**
 * Created by georgehrke on 3/6/16.
 */
public class LocationManager {
    private static List<Location> locations = null;

    /**
     * Setzt den Memory-Cache der Orte zurück. Bei der nächsten Abfrage werden die Orte
     * wieder aus der Datenbank gelesen.
     */
    public static void invalidate() {
        locations = null;
    }

    /**
     * Gibt eine Liste aller Orte zurück.
     * @return Liste von Orten
     */
    public static List<Location> listAll() {
        if (locations == null)
            locations =  Location.listAll(Location.class);
        return locations;
    }

    /**
     * Ermittelt, ob ein Beacon in einem Ort ist.
     * @param b Beacon
     * @param l Ort
     * @return true|false
     */
    public static boolean isBeaconInLocation(Beacon b, Location l) {
        return isPositionInLocation(b.getPosition(), l);
    }

    /**
     * Ermittelt, ob eine Position in einem Ort ist.
     * @param p Position
     * @param l Ort
     * @return true|false
     */
    public static boolean isPositionInLocation(Position p, Location l)
    {
        if(p == null) return false;
        Region r = l.getRegion();
        if (p.getMapName() == null || !p.getMapName().equals(l.getMapName()))
            return false;
        Position x = new Position((int)(p.getX() /*/ l.getScalingFactor()*/), (int)(p.getY() /*/ l.getScalingFactor()*/));
        return r.contains(x.getX(), x.getY());
    }

    /**
     * Gibt eine Liste von Orten zurück, in denen sich der Beacon b befindet.
     * @param b Beacon
     * @return Liste von Orten
     */
    public static List<Location> findLocationsByBeacon(Beacon b) {
        return findLocationsByPosition(b.getPosition());
    }

    /**
     * Gibt eine Liste von Orten zurück, in denen sich die Position p befindet.
     * @param p Position
     * @return Liste von Orten
     */
    public static List<Location> findLocationsByPosition(Position p) {
        List<Location> locations = listAll();
        LinkedList<Location> hit = new LinkedList<>();

        for (Location l: locations) {
            if (isPositionInLocation(p, l)) {
                hit.add(l);
            }
        }
        return hit;
    }

    /**
     * Gibt eine Liste von Orten zurück, die auf der angegebenen Karte platziert sind.
     * @param mapName Karte
     * @return Liste von Orten
     */
    public static List<Location> findLocationsByMapName(String mapName) {
        List<Location> locations = listAll();
        LinkedList<Location> hit = new LinkedList<>();
        if (mapName == null)
            return hit;
        for (Location l: locations) {
            if (mapName.equals(l.getMapName())) {
                hit.add(l);
            }
        }
        return hit;
    }

    public static Location findLocationByName(String name)
    {
        List<Location> locations = listAll();

        for (Location l: locations) {
            if (name.equals(l.getName())) {
                return l;
            }
        }
        return null;
    }
}
