package de.uni_rostock.beacon.bluetooth;

import android.content.Context;
import android.media.MediaScannerConnection;
import android.os.Environment;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.List;

import de.uni_rostock.beacon.Location;
import de.uni_rostock.beacon.MyApplication;

/**
 * @author Johann Bauer
 *         created 08.02.2016
 */
public class Logger {
    private String filename;
    private Gson gson = new GsonBuilder().setPrettyPrinting().create();
    public static final String path = Environment.getExternalStorageDirectory() + "/beacon/";
    private PrintWriter writer;
    private String delimiter = "";

    public static class Observation {
        @SuppressWarnings("unused")
        public Long when = System.currentTimeMillis();
        public List<Beacon> what;
        public Beacon closest;
    }

    public Logger(String filename)  {
        this.filename = filename;
        File mFolder = new File(path);
        if (!mFolder.exists()) {
            mFolder.mkdir();
            mFolder.setReadable(true, true);
            MediaScannerConnection.scanFile(
                    MyApplication.application, new String[]{mFolder.toString()}, null, null);
        }
        try {
            writer = new PrintWriter(path + this.filename, "UTF-8");
            writer.write("[\n");
        }
        catch (FileNotFoundException | UnsupportedEncodingException err) {
            Log.d(this.getClass().getCanonicalName(), err.getLocalizedMessage());
        }

    }

    public Logger() {
        this("beacon_log_" + System.currentTimeMillis() + ".json");
    }

    /**
     * Loggt eine Beobachtung in die angegebene JSON-Datei.
     * @param entry Beobachtung
     */
    public void log(final Observation entry) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                writer.write(delimiter + gson.toJson(entry));
                delimiter = "\n, ";
            }
        }).start();

    }

    /**
     * Schließt die Log-Datei.
     * @return Name der Log-Datei.
     */
    public String commit() {
        writer.write("\n]");
        writer.flush();
        writer.close();
        MediaScannerConnection.scanFile(
                MyApplication.application, new String[]{path + this.filename}, null, null);
        try {
            dumpMeta(path + this.filename);
        } catch (FileNotFoundException | UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return path + filename;
    }

    // Metadaten werden zur Beacon-Auswertung mit Ortsgenauigkeit gebraucht

    public static class MetaDataHolder {
        public List<Location> locations;
        public List<Beacon> beacons;
    }

    /**
     * Schreibt die Metadaten zur Evaluation in eine Datei.
     * @param file Dateiname des Logs
     * @throws FileNotFoundException
     * @throws UnsupportedEncodingException
     */
    public void dumpMeta(String file) throws FileNotFoundException, UnsupportedEncodingException {
        MetaDataHolder mdh = new MetaDataHolder();
        mdh.locations = LocationManager.listAll();
        mdh.beacons = BeaconManager.getPlacedBeacons();

        String s = gson.toJson(mdh);

        writer = new PrintWriter(file + ".meta", "UTF-8");
        writer.write(s);
        writer.flush();
        writer.close();

        MediaScannerConnection.scanFile(
                MyApplication.application, new String[]{file + ".meta"}, null, null);

    }
}
