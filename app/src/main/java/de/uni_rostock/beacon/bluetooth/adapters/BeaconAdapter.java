package de.uni_rostock.beacon.bluetooth.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import de.uni_rostock.beacon.MyApplication;
import de.uni_rostock.beacon.R;
import de.uni_rostock.beacon.bluetooth.Beacon;
import de.uni_rostock.beacon.bluetooth.fragments.BeaconListFragment;

public class BeaconAdapter extends RecyclerView.Adapter {
    private List<Beacon> beacons;
    public BeaconAdapter(List<Beacon> beacons) {
        this.beacons = beacons;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_beacons, parent, false);
        PlacesViewHolder pvh = new PlacesViewHolder(v);
        return pvh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        ((PlacesViewHolder)holder).name.setText(beacons.get(position).getName());
        ((PlacesViewHolder)holder).description.setText(beacons.get(position).getDescription());
        ((PlacesViewHolder)holder).item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BeaconListFragment.detailFragment.setBeacon(beacons.get(position));
                MyApplication.getActivity().showFragment(BeaconListFragment.detailFragment);


            }
        });
    }

    @Override
    public int getItemCount() {
        return beacons.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public static class PlacesViewHolder extends RecyclerView.ViewHolder {
        TextView name;
        TextView description;
        View item;

        PlacesViewHolder(View itemView) {
            super(itemView);
            item = itemView;
            name = (TextView) itemView.findViewById(R.id.name);
            description = (TextView) itemView.findViewById(R.id.description);
        }
    }
}
