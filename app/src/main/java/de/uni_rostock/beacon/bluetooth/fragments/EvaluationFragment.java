package de.uni_rostock.beacon.bluetooth.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.LightingColorFilter;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.malinskiy.materialicons.IconDrawable;
import com.malinskiy.materialicons.Iconify;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import de.uni_rostock.beacon.MyApplication;
import de.uni_rostock.beacon.R;
import de.uni_rostock.beacon.bluetooth.Beacon;
import de.uni_rostock.beacon.bluetooth.BeaconManager;
import de.uni_rostock.beacon.bluetooth.BluetoothLocalization;
import de.uni_rostock.beacon.bluetooth.Logger;
import de.uni_rostock.beacon.fragments.Fragment;

/**
 * @author Johann Bauer
 *         created 08.02.2016
 */
public class EvaluationFragment extends Fragment implements Observer {
    private ArrayList<String> buttons = new ArrayList<>();
    private Logger logger = new Logger();
    private Fragment parent;
    private Beacon closestBeacon = null;
    private Button closestBeaconsButton = null;
    private View v;
    private boolean didClick = false;
    private int i = 0;

    /**
     * Setzt das Fragment, zu dem zurückgekehrt werden soll, wenn dieses Fragment beendet.
     * @param parent Fragment
     */
    public void setParent(Fragment parent) {
        this.parent = parent;
    }

    @Override
    public String getName() {
        return "Schnell-Evaluation (Beacon)";
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        BluetoothLocalization localization = (BluetoothLocalization) MyApplication.getLocalization();
        //localization.start();
        localization.addObserver(this);

        v = inflater.inflate(R.layout.fragment_evaluation, container, false);

        return v;
    }

    @Override
    public void onStart() {
        super.onStart();
        buttons.clear();
    }

    @Override
    public void update(Observable observable, Object o) {
        Logger.Observation observation = new Logger.Observation();
        observation.what = BeaconManager.getActiveBeacons();
        observation.closest = closestBeacon;
        CheckBox onlyOnClick = (CheckBox) v.findViewById(R.id.on_click_only);
        for (final Beacon b: observation.what) {
            if (!buttons.contains(b.getAddress())) {
                final Button button = new Button(MyApplication.getActivity());

                String text = b.getDescription();
                if(!text.equals("Unbenannt"))
                {
                    text += "\u2705";
                    /*text = "";
                    if (b.getId() != null) {
                        text += "\u2705";
                    }*/
                }
                else
                    text = "";
                text += " " + b.getName() + " (" + b.getAddress() + ")\n" +
                        b.getAdditionalID();
                button.setText(text);

                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        didClick = true;
                        if (closestBeaconsButton != null) {
                            closestBeaconsButton.getBackground().setColorFilter(null);
                        }
                        closestBeaconsButton = button;
                        closestBeacon = b;
                        button.getBackground().setColorFilter(new LightingColorFilter(0xff888888, 0x000000));
                        //button.setBackgroundResource(android.R.drawable.btn_default);
                        //button.setF
                        //button.setBackgroundColor(Color.parseColor("#AAAAAA"));
                        String bName = b.getDescription();
                        if(bName.equals("Unbenannt") || bName == null)
                        {
                            bName = b.getName();
                        }
                        Toast.makeText(getActivity(), "Dichtester Beacon ist nun " + bName,
                                Toast.LENGTH_SHORT).show();
                    }
                });
                buttons.add(b.getAddress());
                MyApplication.getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ((LinearLayout) v.findViewById(R.id.rootContainer)).addView(button);
                        v.invalidate();
                    }
                });

            }
        }
        if (closestBeacon != null && (didClick || !onlyOnClick.isChecked())) {
            logger.log(observation);
            didClick = false;
            i++;
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    ((TextView)v.findViewById(R.id.saved_count)).setText(String.valueOf(i));
                }
            });
        }

    }

    @Override
    public boolean onBackButtonPress() {
        if (parent != null) {
            logger = null;
            MyApplication.getActivity().showFragment(parent);
            return true;
        }
        return false;
    }

    @Override
    public String getCategory() {
        return "Evaluation";
    }

    @Override
    public void onInsertMenu(Menu m) {
        MenuItem i = m.add("Speichern");
        i.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                CharSequence options[] = new CharSequence[]{"Ja", "Nein"};

                AlertDialog.Builder skip = new AlertDialog.Builder(getActivity());
                skip.setTitle("Möchtest du die Beacon Evaluation beenden und speichern?");
                skip.setItems(options, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == 0) {
                            String name = logger.commit();
                            closestBeacon = null;
                            logger = new Logger();
                            Toast.makeText(getActivity(), "Ergebnisse gespeichert als " + name,
                                    Toast.LENGTH_LONG).show();
                        }
                        if (which == 1) {
                            // Wenn man "Nein" klickt, schließt sich ledliglich das Fenster
                        }
                    }
                });
                skip.show();
                return true;
            }
        });
        i.setIcon(new IconDrawable(MyApplication.getActivity(), Iconify.IconValue.zmdi_save)
                .colorRes(R.color.md_white_1000)
                .actionBarSize());
        i.setShowAsAction(MenuItem.SHOW_AS_ACTION_WITH_TEXT | MenuItem.SHOW_AS_ACTION_ALWAYS);
    }
}
