package de.uni_rostock.beacon.bluetooth;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * @author Johann Bauer
 *         created 08.02.2016
 */
public class BeaconManager {
    private static TreeMap<String, Beacon> beacons = loadBeacons();

    /**
     * Lädt in der Datenbank gespeicherte Beacons.
     * @return HashMap, die Adressen auf Beacons abbildet
     */
    private static TreeMap<String, Beacon> loadBeacons() {
        beacons = new TreeMap<>();
        // gespeicherte Beacons aus der Datenbank laden
        List<Beacon> placedBeacons = Beacon.listAll(Beacon.class);
        for (Beacon b: placedBeacons) {
            beacons.put(b.getAddress(), b);
        }
        return beacons;
    }

    /**
     * Liefert eine Liste aller Beacons, die bereits im System positioniert sind.
     * @return Liste von Beacons mit Position
     */
    public static List<Beacon> getPlacedBeacons() {
        List<Beacon> placedBeacons = new LinkedList<>();
        for (Map.Entry<String, Beacon> e: beacons.entrySet()) {
            Beacon b = e.getValue();
            if (b.getPosition() != null)
                placedBeacons.add(b);
        }
        return placedBeacons;
    }

    public static List<Beacon> getAddedBeacons() {
        List<Beacon> addedBeacons = new LinkedList<>();
        for (Map.Entry<String, Beacon> e: beacons.entrySet()) {
            Beacon b = e.getValue();
            if (b.getId() != null)
                addedBeacons.add(b);
        }

        return addedBeacons;
    }

    /**
     * Liefert einen Beacon anhand der MAC-Adresse zurück.
     * @param address MAC-Adresse im üblichen Format (hexadezimal, byteweise)
     * @return Beacon mit der angegebenen Adresse
     */
    public static Beacon getBeaconByAddress(String address) {

        if (!beacons.containsKey(address)) {
            beacons.put(address, new Beacon());
        }
        return BeaconManager.beacons.get(address);
    }

    /**
     * Gibt aktive Beacons zurück.
     * @return Liste der aktuell sichtbaren Beacons zurück.
     */
    public static List<Beacon> getActiveBeacons() {
        ArrayList<Beacon> current = new ArrayList<>();
        for (Map.Entry<String, Beacon> b: beacons.entrySet()) {
            if (b.getValue().getValidUntil() != null && b.getValue().getValidUntil() > System.currentTimeMillis())
                current.add(b.getValue());
        }
        return current;
    }

    public static List<Beacon> getActiveUnknownBeacons() {
        ArrayList<Beacon> current = new ArrayList<>();
        for (Map.Entry<String, Beacon> b: beacons.entrySet()) {
            if (b.getValue().getValidUntil() != null && b.getValue().getValidUntil() > System.currentTimeMillis() && b.getValue().getId() == null) {
                current.add(b.getValue());
            }
        }
        return current;
    }

    /**
     * Liefert den Beacon, der aktuell mit der höchsten Signalstärke empfangen wird.
     * @return dichtester Beacon
     */
    public static Beacon getClosestBeacon() {
        int maxRssi = -Integer.MAX_VALUE;
        Beacon closest = null;
        for (Map.Entry<String, Beacon> set: beacons.entrySet()) {
            Beacon b = set.getValue();
            if (b.getRssi() > maxRssi) {
                maxRssi = b.getRssi();
                closest = b;
            }
        }
        return closest;
    }

    /**
     * Liefert den Beacon, der aktuell mit der höchsten Signalstärke empfangen wird.
     * @return dichtester Beacon
     */
    public static Beacon getClosestPositionedBeacon() {
        int maxRssi = -Integer.MAX_VALUE;
        Beacon closest = null;
        for (Map.Entry<String, Beacon> set: beacons.entrySet()) {
            Beacon b = set.getValue();
            if (b.getRssi() > maxRssi && b.getPosition() != null
                    && b.getValidUntil() != null && b.getValidUntil() > System.currentTimeMillis()) {
                maxRssi = b.getRssi();
                closest = b;
            }
        }
        return closest;
    }

    /**
     * Setzt den Cache zurück, indem wieder neue Daten aus der Datenbank geladen werden.
     */
    public static void flush() {
        beacons = loadBeacons();
    }


}
