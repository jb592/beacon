package de.uni_rostock.beacon;


import java.util.List;
import java.util.Observer;

import de.uni_rostock.beacon.fragments.Fragment;

/**
 * @author Johann Bauer
 */
public interface ILocalization {
    /**
     * Gibt die Position zurück, an der sich das Gerät aktuell befindet.
     * Kann die Position nicht bestimmt werden, sollte null zurückgegeben werden.
     * @return Position
     */
    Position getPosition();

    /**
     * Fügt einen Observer zur Liste der Observer hinzu.
     * Die Observer müssen aufgerufen werden, sobald sich die Position ändert (und nur dann).
     * @see java.util.Observable
     * @see de.uni_rostock.beacon.bluetooth.BluetoothLocalization
     * @param o Observer
     */
    void addOnPositionChangeListener(Observer o);
    void removeOnPositionChangeListener(Observer o);

    /**
     * Falls die Lokalisierung zusätzliche Fragmente ("Menüeinträge") etwa zur Konfiguration
     * benötigt, soll diese Methode eine Liste dieser Fragmente zurückgeben.
     * Werden keine Fragmente benötigt, ist eine leere Liste zurückzugeben.
     * @see Fragment
     * @return Liste von Fragmenten
     */
    List<Fragment> getAdditionalFragments();

    /**
     * Startet die Lokalisierung. Wird bei jedem Start der App automatisch aufgerufen.
     * Dazu gehört auch das Reaktivieren der App, nachdem das Display ausgegangen ist.
     */
    void start();

    /**
     * Beendet die Lokalisierung. Wird bei jedem Pausieren der App aufgerufen.
     */
    void stop();

    /**
     * Sobald der App die reale Position des Nutzers bekannt ist, wird diese Methode aufgerufen.
     * In der Beacon-Lokalisierung wird das dazu verwendet, ein Log anzulegen.
     * @param real Position an der sich der Nutzer gerade befindet
     */
    void onSetRealPosition(Position real);

    /**
     * Diese Methode wird aufgerufen, sobald ein Messlauf beendet ist.
     * Das wird in der Beacon-Evaluation zum Trennen der Logs verwendet.
     */
    void onObservationEnd();
}
