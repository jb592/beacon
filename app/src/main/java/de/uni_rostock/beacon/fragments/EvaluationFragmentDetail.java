package de.uni_rostock.beacon.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import de.uni_rostock.beacon.EvaluationController;
import de.uni_rostock.beacon.MapData;
import de.uni_rostock.beacon.MyApplication;
import de.uni_rostock.beacon.R;
import de.uni_rostock.beacon.TestRun;

/**
 * Created by Tom on 01.03.2016.
 */
public class EvaluationFragmentDetail extends Fragment{
    private View v;
    private Fragment parent;
    private TextView name;
    private EditText neuerName;
    private TestRun testRun;

    public void setParent(Fragment parent) {
        this.parent = parent;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_evaluation_detail, container, false);
        onAfterCreateView(v);

        neuerName = (EditText) v.findViewById(R.id.neuerName);


        (v.findViewById(R.id.delete_Evaluation)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CharSequence options[] = new CharSequence[]{"Ja", "Nein"};

                AlertDialog.Builder confirm = new AlertDialog.Builder(getActivity());
                confirm.setTitle("Möchtest du diesen Testlauf wirklich löschen?");
                confirm.setItems(options, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == 0) {
                            EvaluationFragmentMain.evaluationController.removeTestRun(testRun);

                            // Da es den gerade betrachteten Testlauf nun nicht mehr gibt, wird man zurück in den ElternFragment gebracht
                            if (parent != null) {
                                MyApplication.getActivity().showFragment(parent);
                                return;
                            }
                            return;
                        }
                        if (which == 1) {
                            // Wenn man auf "nein" klickt, löscht man den Testlauf nicht
                        }

                    }
                });
                confirm.show();
            }
        });

        (v.findViewById(R.id.show_Test_Places)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Die Testortansicht soll nur aufgerufen werden können, wenn auch die entsprechende Karte bekannt ist
                if(EvaluationFragmentMain.testPlacesFragment.setMapData(testRun.getMapName()))
                {
                    EvaluationFragmentMain.testPlacesFragment.setTestRun(testRun);
                    MyApplication.getActivity().showFragment(EvaluationFragmentMain.testPlacesFragment);
                }

            }
        });


        // endDate.setText(testRun.getEndDate().toString());
        if (testRun != null)
            setTestRun(testRun);
        return v;
    }

    @Override
    public void onStart() {
        super.onStart();
        neuerName.setText(testRun.getName());
        (v.findViewById(R.id.change)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String description = neuerName.getText().toString();
                testRun.setName(description);
                testRun.save();
                if (name != null) name.setText(testRun.getName());
            }
        });
    }

    public void setTestRun(TestRun cur) {
        this.testRun = cur;
        if (v != null) {
            name = (TextView) v.findViewById(R.id.name);
            name.setText(cur.getName());

            TextView testLocations = (TextView) v.findViewById(R.id.count);
            if(testRun.getTestPlaces().size() == 1)
            {
                testLocations.setText("1 aufgezeichneter Testort");
            }
            else testLocations.setText(testRun.getTestPlaces().size() + " aufgezeichnete Testorte");

            TextView startDate = (TextView) v.findViewById(R.id.startDate);
            startDate.setText(testRun.getStartDate().toString());

            TextView duration = (TextView) v.findViewById(R.id.duration);
            duration.setText("Keine Angabe der Dauer möglich");
            if(testRun.getStartDate() != null && testRun.getEndDate() != null)
            {
                long diff = testRun.getEndDate().getTime() - testRun.getStartDate().getTime();
                long minutes = TimeUnit.MILLISECONDS.toMinutes(diff);
                long seconds = TimeUnit.MILLISECONDS.toSeconds(diff) - (minutes * 60);  // Damit die Sekunden 60 nicht überschreiten

                TextView accuracy = (TextView) v.findViewById(R.id.textViewAccuracy);
                accuracy.setText(testRun.getAccuracy() + "%");

                if(minutes == 0)
                {
                    duration.setText(seconds + " Sekunden");
                }
                else
                {
                    duration.setText(minutes + " Minuten und " + seconds + " Sekunden");
                }
            }




            // ((ImageView) v.findViewById(R.id.mapImage)).setImageBitmap(cur.getScaledBitmap());
        }


    }

    @Override
    public boolean onBackButtonPress() {
        if (parent != null) {
            MyApplication.getActivity().showFragment(parent);
            return true;
        }
        return false;
    }

    @Override
    public String getName() {
        /*String name = "Testlauf";
        if (testRun != null) {
            name += " \"" + testRun.getName() + "\"";
        }
        return name;*/
        return "Details einer Evaluation";
    }

}