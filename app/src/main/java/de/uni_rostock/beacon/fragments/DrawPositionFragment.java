package de.uni_rostock.beacon.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.malinskiy.materialicons.IconDrawable;
import com.malinskiy.materialicons.Iconify;

import de.uni_rostock.beacon.Location;
import de.uni_rostock.beacon.MapData;
import de.uni_rostock.beacon.MyApplication;
import de.uni_rostock.beacon.R;
import de.uni_rostock.beacon.views.DrawingView;

/**
 * Created by bauerj on 25.02.2016.
 */
public class DrawPositionFragment extends Fragment {
    private MapData mapData;
    private Location location;
    private ProgressBar progressBar;
    private LocationDetailFragment parent;
    private View v;
    private DrawingView drawingView;
    private ImageView mapImage;
    private String name = null;
    private Boolean saved = false;

    public void setParent(Fragment parent) {
        this.parent = (LocationDetailFragment) parent;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_paint_region, container, false);
        drawingView = (DrawingView) v.findViewById(R.id.drawing);
        progressBar = (ProgressBar) v.findViewById(R.id.progressBar);
        v.findViewById(R.id.mode_add).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawingView.setMode(DrawingView.Mode.DRAW_REGION_ADD);
            }
        });
        v.findViewById(R.id.mode_del).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawingView.setMode(DrawingView.Mode.DRAW_REGION_REMOVE);
            }
        });
        drawingView.setAddButton((Button) v.findViewById(R.id.mode_add));
        drawingView.setRemoveButton((Button) v.findViewById(R.id.mode_del));
        drawingView.setVisibility(View.INVISIBLE);
        progressBar.setVisibility(View.VISIBLE);
        drawingView.setVisibility(View.VISIBLE);
        drawingView.setMapData(mapData);
        progressBar.setVisibility(View.INVISIBLE);
        if (location != null) {
            setLocation(location);
        }
        Toast.makeText(getActivity(), "Benutzen Sie den Stift, um den Umriss des Orts einzuzeichnen.", Toast.LENGTH_LONG).show();
        return v;
    }

    public void setMapData(MapData mapData) {
        this.mapData = mapData;
    }

    public void setLocation(Location location) {
        this.location = location;
        saved = false;
        if (drawingView == null)
            return;
        if (location.hasRegion() && location.getRegion() != null) {
            //drawingView.clearRegionsToDraw();
            drawingView.setEditableRegion(location.getRegion());
            drawingView.setMode(DrawingView.Mode.DRAW_REGION_ADD);
        }
        else {
            drawingView.setMode(DrawingView.Mode.DRAW_REGION_INITIAL);
        }
    }

    @Override
    public String getName() {
        if (name != null)
            return "Position für " + name + " festlegen";
        return "Position festlegen";
    }

    @Override
    public boolean onBackButtonPress() {
        if (parent != null) {
            parent.setName(name);
            MyApplication.getActivity().showFragment(parent);
            name = null;
            Toast.makeText(getActivity(), "Positionieren abgebrochen.", Toast.LENGTH_SHORT).show();
            return true;
        }
        return false;
    }

    @Override
    public void onInsertMenu(Menu m) {
        MenuItem i = m.add("Übernehmen");
        i.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                location.setRegion(drawingView.getRegion());
                location.setScalingFactor((float) mapData.getWidth() / drawingView.getWidth());
                parent.setName(name);
                saved = true;
                MyApplication.getActivity().showFragment(parent);
                name = null;
                return true;
            }
        });
        i.setIcon(new IconDrawable(MyApplication.getActivity(), Iconify.IconValue.zmdi_save)
                .colorRes(R.color.md_white_1000)
                .actionBarSize());
        i.setShowAsAction(MenuItem.SHOW_AS_ACTION_WITH_TEXT | MenuItem.SHOW_AS_ACTION_ALWAYS);
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean needsSave() {
        return !saved;
    }
}
