package de.uni_rostock.beacon.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import de.uni_rostock.beacon.MyApplication;
import de.uni_rostock.beacon.adapters.EvaluationAdapter;
import de.uni_rostock.beacon.EvaluationController;
import de.uni_rostock.beacon.R;

/**
 * Created by Tom on 14.02.2016.
 */
public class EvaluationFragmentMain extends Fragment{
    public static EvaluationController evaluationController;
    private View v;

    public static EvaluationFragmentDetail detailFragment = new EvaluationFragmentDetail();
    public static EvaluationFragmentTestPlaces testPlacesFragment = new EvaluationFragmentTestPlaces();
    public static EvaluationFragmentStyle styleFragment = new EvaluationFragmentStyle();

    // Fragmente, die die verschiedenen Arten an Testläufen darstellen
    public static EvaluationFragmentRandom testRunFragment = new EvaluationFragmentRandom();

    @Override
    public String getName() {
        return "Genauigkeit auf Ortsebene";
    }

    @Override
    public String getCategory() {
        return "Evaluation";
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        evaluationController = new EvaluationController();
        v = inflater.inflate(R.layout.fragment_evaluationmain, container, false);

        RecyclerView rv = (RecyclerView)v.findViewById(R.id.rv);
        rv.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        rv.setLayoutManager(llm);
        EvaluationAdapter adapter = new EvaluationAdapter(evaluationController.getTestRuns());
        rv.setAdapter(adapter);
        v.findViewById(R.id.starte).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getActivity(), "Neuer Testlauf",
                        Toast.LENGTH_SHORT).show();
                MyApplication.getActivity().showFragment(styleFragment);
            }
        });
        detailFragment.setParent(this);
        testPlacesFragment.setParent(detailFragment);
        styleFragment.setParent(this);

        // Die Einzelnen Testlaeufe sollen beim Betaetigen des Zurück-Button auch direkt ins Hauptmenue der Evaluation gelangen
        testRunFragment.setParent(this);

        testRunFragment.setEvaluationController(evaluationController);
        return v;
    }

}
