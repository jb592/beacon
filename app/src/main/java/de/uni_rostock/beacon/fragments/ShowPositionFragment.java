package de.uni_rostock.beacon.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.List;
import java.util.Objects;
import java.util.Observable;
import java.util.Observer;

import de.uni_rostock.beacon.ILocalization;
import de.uni_rostock.beacon.Location;
import de.uni_rostock.beacon.MapData;
import de.uni_rostock.beacon.MapDataManager;
import de.uni_rostock.beacon.MyApplication;
import de.uni_rostock.beacon.Position;
import de.uni_rostock.beacon.R;
import de.uni_rostock.beacon.bluetooth.LocationManager;
import de.uni_rostock.beacon.views.DrawingView;



public class ShowPositionFragment extends Fragment implements Observer {

    private View v;
    private DrawingView mapImage;
    private MapData selectedMapData;
    private MapDataManager mapDataManager = new MapDataManager();
    private ProgressBar progressBar;
    private Position lastPosition;
    private ILocalization localizationInterface = MyApplication.getLocalization();
    private TextView explaination;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_show_position, container, false);
        selectedMapData = null;
        lastPosition = null;
        localizationInterface.addOnPositionChangeListener(this);
        mapImage = (DrawingView) v.findViewById(R.id.map_image);
        progressBar = (ProgressBar) v.findViewById(R.id.progressBar);
        mapImage.setMode(DrawingView.Mode.DRAW_NONE);
        explaination = (TextView) v.findViewById(R.id.explaination);
        return v;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        localizationInterface.removeOnPositionChangeListener(this);
    }


    @Override
    public String getName() {
        return "Eigene Position bestimmen";
    }

    @Override
    public String getCategory() {
        return "Allgemein";
    }


    int dots = 0;
    String text;

    @Override
    public void update(Observable observable, Object data) {
        final Position current = localizationInterface.getPosition();
        if (current == null) {
            text = "Aktuell keine Positionsbestimmung möglich.";
            for (int i = 0; i < (dots % 3); i++) {
                text += ".";
            }
            dots++;
            MyApplication.getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    explaination.setText(text);
                }
            });
            //return;
        }
        else {
            MyApplication.getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    explaination.setText("");
                }
            });
        }
        if (current != null && selectedMapData == null || current != null && !Objects.equals(selectedMapData.getName(), current.getMapName())) {
            selectedMapData = mapDataManager.getMapData(current.getMapName());
            final List<Location> locations = LocationManager.findLocationsByMapName(current.getMapName());
            MyApplication.getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mapImage.setVisibility(View.INVISIBLE);
//                    progressBar.setVisibility(View.VISIBLE);
                    mapImage.setMapData(selectedMapData);
                    for (Location l: locations)
                        mapImage.addRegionToDraw(l.getRegion(), l.getName());
                    mapImage.setVisibility(View.VISIBLE);
 //                   progressBar.setVisibility(View.INVISIBLE);

                }
            });
        }
        if (lastPosition != current) {
            MyApplication.getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mapImage.setPositionToDraw(current);
                }
            });
            lastPosition = current;
        }

    }
}
