package de.uni_rostock.beacon.fragments;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import de.uni_rostock.beacon.MapData;
import de.uni_rostock.beacon.MyApplication;
import de.uni_rostock.beacon.R;

/**
 * Created by georgehrke on 2/19/16.
 */
public class MapDetailFragment extends Fragment {

    private View v;
    private MapData map;
    private Fragment parent;

    public void setParent(Fragment parent) {
        this.parent = parent;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_map_detail, container, false);
        if (map != null)
            setMapData(map);
        return v;
    }

    public void setMapData(MapData cur) {
        this.map = cur;
        if (v != null) {
            TextView name = (TextView) v.findViewById(R.id.name);
            name.setText(cur.getName());
            ((ImageView) v.findViewById(R.id.mapImage)).setImageBitmap(cur.getScaledBitmap());
        }
    }

    @Override
    public String getName() {
        String name = "Lageplan";
        if (map != null) {
            name += " \"" + map.getName() + "\"";
        }
        return name;
    }

    @Override
    public boolean onBackButtonPress() {
        if (parent != null) {
            MyApplication.getActivity().showFragment(parent);
            return true;
        }
        return false;
    }
}
