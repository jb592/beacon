package de.uni_rostock.beacon.fragments;


import android.os.Bundle;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.orm.SugarContext;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.LinkedList;
import java.util.List;

import de.uni_rostock.beacon.MyApplication;
import de.uni_rostock.beacon.R;
import de.uni_rostock.beacon.bluetooth.BeaconManager;
import de.uni_rostock.beacon.bluetooth.LocationManager;

public class ProjectFragment extends Fragment {
    private View v;
    private String DB_PATH;
    private final String DB_NAME = "Sugar.db";
    private static final String EXTERNAL_PATH = Environment.getExternalStorageDirectory() + "/beacon/projects/";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_project, container, false);
        onAfterCreateView(v);

        DB_PATH = MyApplication.getActivity().getApplicationInfo().dataDir + "/databases/";


        final Spinner projectsSpinner = (Spinner) v.findViewById(R.id.spinner);
        List<String> projectNames = getSavedProjects();
        ArrayAdapter adapter = new ArrayAdapter<>(MyApplication.getActivity(), android.R.layout.simple_spinner_dropdown_item, projectNames);
        projectsSpinner.setAdapter(adapter);
        v.findViewById(R.id.buttonImport).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                importDB((String) projectsSpinner.getSelectedItem());
            }
        });
        final EditText _editText = (EditText)v.findViewById(R.id.exportName);
        v.findViewById(R.id.export).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = (_editText).getText().toString();
                exportDB(name);
                List<String> projectNames = getSavedProjects();
                ArrayAdapter adapter = new ArrayAdapter<>(MyApplication.getActivity(), android.R.layout.simple_spinner_dropdown_item, projectNames);
                projectsSpinner.setAdapter(adapter);
            }
        });
        v.findViewById(R.id.reset).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clearDB();
            }
        });
        return v;
    }

    public List<String> getSavedProjects() {
        File exported = new File(EXTERNAL_PATH);
        if (!exported.exists()) {
            exported.mkdirs();
        }
        File[] projects = exported.listFiles();
        LinkedList<String> projectNames = new LinkedList<>();
        for (File f: projects) {
            if (!f.getName().contains(".sqlite3"))
                continue;
            projectNames.add(f.getName().replace(".sqlite3", ""));
        }
        return projectNames;
    }

    @Override
    public String getName() {
        return "Projekte";
    }

    @Override
    public String getCategory() {
        return "Verwalten";
    }

    @Override
    public Integer getMenuIndex() {
        return super.getMenuIndex();
    }

    public void exportDB(String name) {
        File db = new File(DB_PATH + DB_NAME);
        File dest = new File(EXTERNAL_PATH + name + ".sqlite3");
        try {
            copyFile(db, dest);
        }
        catch (IOException e) {
            Toast.makeText(MyApplication.getActivity(),
                    "Beim Exportieren der Datei ist ein Fehler aufgetreten: " + e.getLocalizedMessage(),
                    Toast.LENGTH_LONG).show();
        }
        Toast.makeText(MyApplication.getActivity(),
                "Projekt erfolgreich exportiert.",
                Toast.LENGTH_LONG).show();
    }

    public void importDB(String name) {
        File db = new File(DB_PATH + DB_NAME);
        File dest = new File(EXTERNAL_PATH + name + ".sqlite3");
        try {
            copyFile(dest, db);
        }
        catch (IOException e) {
            Toast.makeText(MyApplication.getActivity(),
                    "Beim Importieren der Datei ist ein Fehler aufgetreten: " + e.getLocalizedMessage(),
                    Toast.LENGTH_LONG).show();
        }
        Toast.makeText(MyApplication.getActivity(),
                "Projekt erfolgreich importiert.",
                Toast.LENGTH_LONG).show();
        flushDBCache();
    }

    public void clearDB() {
        File db = new File(DB_PATH + DB_NAME);
        if (!db.delete()) {
            Toast.makeText(MyApplication.getActivity(),
                    "Die Datenbank konnte nicht zurückgesetzt werden.",
                    Toast.LENGTH_LONG).show();
        }
        else
            Toast.makeText(MyApplication.getActivity(),
                "Neues Projekt angelegt.",
                Toast.LENGTH_LONG).show();
        flushDBCache();
    }

    public void flushDBCache() {
        LocationManager.invalidate();
        BeaconManager.flush();
        if(EvaluationFragmentMain.evaluationController != null)
            EvaluationFragmentMain.evaluationController.reloadData();
        // Sugar soll die Datenbank wieder in Ordnung bringen
        SugarContext.terminate();
        SugarContext.init(MyApplication.application);
    }

    public static void copyFile(File sourceFile, File destFile) throws IOException {
        if (!destFile.getParentFile().exists())
            destFile.getParentFile().mkdirs();

        if (!destFile.exists()) {
            destFile.createNewFile();
        }

        FileChannel source = null;
        FileChannel destination = null;

        try {
            source = new FileInputStream(sourceFile).getChannel();
            destination = new FileOutputStream(destFile).getChannel();
            destination.transferFrom(source, 0, source.size());
        } finally {
            if (source != null) {
                source.close();
            }
            if (destination != null) {
                destination.close();
            }
        }
    }
}
