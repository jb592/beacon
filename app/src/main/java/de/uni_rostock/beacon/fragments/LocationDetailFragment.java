package de.uni_rostock.beacon.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import com.malinskiy.materialicons.IconDrawable;
import com.malinskiy.materialicons.Iconify;

import java.util.List;

import de.uni_rostock.beacon.Location;
import de.uni_rostock.beacon.MapData;
import de.uni_rostock.beacon.MapDataManager;
import de.uni_rostock.beacon.MyApplication;
import de.uni_rostock.beacon.R;
import de.uni_rostock.beacon.bluetooth.LocationManager;


public class LocationDetailFragment extends Fragment {

    private View v;
    private Location l;
    private Fragment parent;
    private final List<MapData> maps = new MapDataManager().getMapDatas();
    private Spinner spinner;
    private DrawPositionFragment drawPositionFragment = new DrawPositionFragment();
    private EditText name;
    private String nameText;
    private boolean openingDrawing = false;

    public void setParent(Fragment parent) {
        this.parent = parent;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_location_detail, container, false);
        onAfterCreateView(v);
        ((EditText) v.findViewById(R.id.name)).setText("");
        if (l != null)
            setLocation(l);

        (v.findViewById(R.id.set_position)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawPositionFragment.setMapData(maps.get(spinner.getSelectedItemPosition()));
                drawPositionFragment.setLocation(l);
                openingDrawing = true;
                MyApplication.getActivity().showFragment(drawPositionFragment);
                drawPositionFragment.setName(name.getText().toString());
                openingDrawing = false;
            }
        });
        drawPositionFragment.setParent(this);

        return v;
    }

    public void setLocation(Location l) {
        this.l = l;
        if (v != null) {
            MyApplication.getActivity().setTitle(getName());
            name = (EditText) v.findViewById(R.id.name);
            name.setText(l.getName());
            spinner = (Spinner) v.findViewById(R.id.map);
             // Create an ArrayAdapter using the string array and a default spinner layout
            ArrayAdapter<MapData> adapter = new ArrayAdapter<>(
                    MyApplication.getActivity(), android.R.layout.simple_spinner_dropdown_item, maps);
            // Apply the adapter to the spinner
            spinner.setAdapter(adapter);
            int i = 0;
            for (MapData d: maps) {
                if (d.getName().equals(l.getMapName()))
                    break;
                i++;
            }
            if (i < maps.size())
                spinner.setSelection(i);
        }
    }


    @Override
    public void onStart() {
        super.onStart();
        name = (EditText) v.findViewById(R.id.name);
        if (nameText != null)
            name.setText(nameText);
        else
            name.setText((l != null) ? l.getName() : "");
    }

    public void setName(String text) {
        nameText = text;
    }

    @Override
     public boolean onBackButtonPress() {
        if (parent != null) {
            MyApplication.getActivity().showFragment(parent);
            return true;
        }
        return false;
    }

    @Override
    public String getName() {
        if (l != null && l.getName() != null)
            return "Ort " + l.getName();
        return "Neuen Ort anlegen";
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        nameText = null;
    }

    @Override
    public boolean needsSave() {
        if (openingDrawing)
            return false;
        return l == null
                || !(name.getText().toString().equals(l.getName()))
                || !l.getMapName().equals(maps.get(spinner.getSelectedItemPosition()).getName());
    }

    @Override
    public void onInsertMenu(Menu m) {
        MenuItem i = m.add("Speichern");
        i.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                l.setMapName(maps.get(spinner.getSelectedItemPosition()).getName());
                l.setName((name.getText().toString()));
                l.save();
                LocationManager.invalidate();
                getActivity().onBackPressed();
                return true;
            }
        });
        i.setIcon(new IconDrawable(MyApplication.getActivity(), Iconify.IconValue.zmdi_save)
                .colorRes(R.color.md_white_1000)
                .actionBarSize());
        i.setShowAsAction(MenuItem.SHOW_AS_ACTION_WITH_TEXT | MenuItem.SHOW_AS_ACTION_ALWAYS);
    }
}
