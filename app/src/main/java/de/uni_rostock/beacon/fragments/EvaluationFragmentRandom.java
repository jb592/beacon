package de.uni_rostock.beacon.fragments;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.malinskiy.materialicons.IconDrawable;
import com.malinskiy.materialicons.Iconify;

import java.util.LinkedList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import de.uni_rostock.beacon.EvaluationController;
import de.uni_rostock.beacon.ILocalization;
import de.uni_rostock.beacon.Location;
import de.uni_rostock.beacon.MapData;
import de.uni_rostock.beacon.MapDataManager;
import de.uni_rostock.beacon.MyApplication;
import de.uni_rostock.beacon.Position;
import de.uni_rostock.beacon.R;
import de.uni_rostock.beacon.TestPlace;
import de.uni_rostock.beacon.bluetooth.LocationManager;
import de.uni_rostock.beacon.views.DrawingView;

/**
 * Created by Tom on 14.02.2016.
 */
public class EvaluationFragmentRandom extends Fragment implements Observer {
    private View v;
    private Fragment parent;
    private MapData selectedMapData;
    private MapDataManager mapDataManager = new MapDataManager();
    private Position realPosition = new Position();
    private Position locaPosition;
    private ILocalization localizationInterface = MyApplication.getLocalization();
    private EvaluationController evaluationController;
    private DrawingView mapDrawing;
    private CheckBox debugBox;

    private List<Location> locations;

    // Zeitvariablen, die verhindert, dass man den Speichern-Button zu schnell hintereinander drückt.
    private long mLastClickTime = 0;
    private int buttonDetentionTime = 1000;     // Bestimmt, wir lange der Speichern-Button deaktiviert ist, nachdem man auf ihn gedrückt hat (in Millisekunden)

    // Bestimmt, wie die Postionen bestimmt werden, die während des Testlaufs vom Nutzer passiert werden sollen
    private boolean choosePosition = false;

    // Bestimmt, ob zusätzliche Informationen für Testzwecke angezeigt werden sollen
    private boolean debug = false;

    private ProgressBar progressBar;

    // Hier wird als kleiner Text stets angezeigt, wie viele Testorte bereits aufgezeichnet wurden
    TextView count;

    // Hier werden zusätzliche Informationen zu debug-Zwecken angezeigt
    TextView debugRealPosition;
    TextView debugRealLoca;
    TextView debugLocaPosition;
    TextView debugLocaLoca;

    public void setParent(Fragment parent) {
        this.parent = parent;
    }

    public void setChooseMode(boolean newMode)
    {
        choosePosition = newMode;
    }

    public void setEvaluationController(EvaluationController controller)
    {
        evaluationController = controller;
    }

    public void setMapData(String m)
    {
        selectedMapData = mapDataManager.getMapData(m);
    }

    @Override
    public String getName() {
        if(choosePosition)
            return "Benutzerdefinierte Evaluation";
        else
            return "Zufällige Evaluation";
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_evaluation_random, container, false);

        progressBar = (ProgressBar) v.findViewById(R.id.loading);

        localizationInterface.addOnPositionChangeListener(this);

        // Initialisieren der Textviews
        count = (TextView) v.findViewById(R.id.countTestplace);
        debugRealPosition = (TextView) v.findViewById(R.id.debugRealPosition);
        debugRealLoca = (TextView) v.findViewById(R.id.debugRealLoca);
        debugLocaPosition = (TextView) v.findViewById(R.id.debugLocaPosition);
        debugLocaLoca = (TextView) v.findViewById(R.id.debugLocaLoca);

        debugBox = (CheckBox) v.findViewById(R.id.debugBox);
        v.findViewById(R.id.debugBox).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                debug = debugBox.isChecked();
                debugToggle();
                updateFragment();
            }
        });

        debugToggle();

        v.findViewById(R.id.debugRefresh).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                debugRefresh();
                updateFragment();
            }
        });

        // Initialisieren der Standardbutton
        v.findViewById(R.id.skip).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setRandomTestPosition();
                Toast.makeText(getActivity(), "Neue zufällige Position",
                        Toast.LENGTH_SHORT).show();
            }
        });
        v.findViewById(R.id.confirm).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();

                saveThePlace();
            }
        });



        mapDrawing = (DrawingView) v.findViewById(R.id.drawing_image);
        locations = LocationManager.findLocationsByMapName(selectedMapData.getName());
        MyApplication.getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mapDrawing.setVisibility(View.INVISIBLE);
                progressBar.setVisibility(View.VISIBLE);
                mapDrawing.setMapData(selectedMapData);
                for (Location l: locations)
                    mapDrawing.addRegionToDraw(l.getRegion(), l.getName());
                mapDrawing.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.INVISIBLE);

            }
        });

        mapDrawing.setVisibility(View.VISIBLE);
        mapDrawing.setMapData(selectedMapData);
        if(choosePosition) {
            mapDrawing.setMode(DrawingView.Mode.DRAW_POINT);
        }
        else {
            mapDrawing.setMode(DrawingView.Mode.DRAW_NONE);
        }
        setRandomTestPosition();

        return v;
    }

    @Override
    public void onStart() {
        super.onStart();
        if(selectedMapData != null) evaluationController.startTestRun(selectedMapData.getName());
        else evaluationController.startTestRun("unbekannt");
    }

    /**
     * Speichert die momentan Daten als Testort ab und fügt diesen dem aktuellen Testlauf hinzu
     *
     * @return      void
     */
    private void saveThePlace()
    {
        if(choosePosition)
        {
            realPosition = mapDrawing.getPositionToDraw();
            realPosition.setMapName(selectedMapData.getName());
        }
        if(realPosition == null)
        {
            Toast.makeText(getActivity(), "Es ist noch keine Zielposition festgelegt",
                    Toast.LENGTH_SHORT).show();
            return;
        }

        locaPosition = localizationInterface.getPosition();

        List<String> realPlaces = new LinkedList<>();
        for(Location l : LocationManager.findLocationsByPosition(realPosition))
        {
            realPlaces.add(l.getName());
        }
        List<String> locaPlaces = new LinkedList<>();
        for(Location l : LocationManager.findLocationsByPosition(locaPosition))
        {
            locaPlaces.add(l.getName());
        }
        TestPlace testPlace = new TestPlace(realPosition, locaPosition, realPlaces, locaPlaces, evaluationController.getAktuellerTestRun());
        evaluationController.addTestPlace(testPlace);
        localizationInterface.onSetRealPosition(realPosition);

        updateFragment();
        Toast.makeText(getActivity(), "Testort erfolgreich aufgenommen",
                Toast.LENGTH_SHORT).show();
        setRandomTestPosition();
    }

    /**
     * Setzt die Position, zu der sich der Nutzer als nächstes bewegen soll auf eine zufäliige Position
     *
     * @return      void
     */
    private void setRandomTestPosition()
    {

        realPosition.setMapName(selectedMapData.getName());
        Bitmap bm = selectedMapData.getScaledBitmap();
        realPosition.setX((int) (Math.random() * selectedMapData.getWidth()));
        realPosition.setY((int) (Math.random() * selectedMapData.getHeight()));
        // Das muss später raus
        /*if(LocationManager.findLocationsByPosition(realPosition).size() == 0)
        {
            setRandomTestPosition();
            return;
        }*/
        // Also bis hier hin
        mapDrawing.setPositionToDraw(realPosition);

        // mapDrawing.addRegionToDraw();
        updateFragment();
    }

    @Override
    public void onDestroyView() {
        evaluationController.abortTestRun();
        localizationInterface.removeOnPositionChangeListener(this);
        super.onDestroyView();

    }

    @Override
    public boolean onBackButtonPress() {
        if (parent != null) {
            evaluationController.abortTestRun();
            MyApplication.getActivity().showFragment(parent);
            return true;
        }
        return false;
    }


    @Override
    public void onInsertMenu(Menu m) {
        MenuItem i = m.add("Speichern");
        i.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                CharSequence options[] = new CharSequence[]{"Ja", "Nein"};

                AlertDialog.Builder skip = new AlertDialog.Builder(getActivity());
                skip.setTitle("Möchtest du den Testlauf beenden und speichern?");
                skip.setItems(options, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == 0) {
                            evaluationController.finishTestRun();
                            localizationInterface.onObservationEnd();
                            Toast.makeText(getActivity(), "Testlauf erfolgreich abgeschlossen",
                                    Toast.LENGTH_SHORT).show();
                            MyApplication.getActivity().showFragment(parent);
                        }
                        if (which == 1) {
                            // Wenn man "Nein" klickt, schließt sich ledliglich das Fenster
                        }
                    }
                });
                skip.show();
                return true;
            }
        });
        i.setIcon(new IconDrawable(MyApplication.getActivity(), Iconify.IconValue.zmdi_save)
                .colorRes(R.color.md_white_1000)
                .actionBarSize());
        i.setShowAsAction(MenuItem.SHOW_AS_ACTION_WITH_TEXT | MenuItem.SHOW_AS_ACTION_ALWAYS);
    }

    /**
     * Aktualisiert die Anzeigt über die Anzahl der Testorte und den Inhalt einiger Debug-Textfelder,
     * deren Inhalt sich nur bei Interaktion durch den Nutzer ändern
     *
     * @return      void
     */
    private void updateFragment()
    {
        count.setText("Anzahl der Testorte: " + evaluationController.getAktuellerTestRun().getTestPlaces().size());

        // Hier werden Debug-Informationen aktualisiert
        if(!debug) return;

        if(choosePosition)
        {
            realPosition = mapDrawing.getPositionToDraw();
            realPosition.setMapName(selectedMapData.getName());
        }

        // Hier werden X und Y Koordinaten der Position angezeigt, an die sich der Nutzer bewegen soll
        if(realPosition != null)
            debugRealPosition.setText("Position: X=" + realPosition.getX() + " Y=" + realPosition.getY());

        // Hier wird nur der erste Ort angezeigt, falls die Position in mehreren Orten liegt
        List<Location> realOrte = LocationManager.findLocationsByPosition(realPosition);
        String gesuchterOrt = "";
        if(realOrte.size() > 0) gesuchterOrt = realOrte.get(0).getName();
        debugRealLoca.setText("Ort: " + gesuchterOrt);
    }

    /**
     * Aktualisiert den Inhalt einiger Debug-Textfelder, deren Inhalt sich jederzeit ändern kann
     *
     * @return      void
     */
    private void debugRefresh()
    {
        locaPosition = localizationInterface.getPosition();
        if (locaPosition != null)
            debugLocaPosition.setText("Position: X=" + locaPosition.getX() + " Y=" + locaPosition.getY());

        // Hier wird nur der erste Ort angezeigt, falls die Position in mehreren Orten liegt
        List<Location> locaOrte = LocationManager.findLocationsByPosition(locaPosition);
        String aktuellerOrt = "";
        if (locaOrte.size() > 0) aktuellerOrt = locaOrte.get(0).getName();
        debugLocaLoca.setText("Ort: " + aktuellerOrt);
        if (locaPosition == null) {
            Toast.makeText(getActivity(), "Fehler: Position konnte nicht abgerufen werden",
                    Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(getActivity(), "Position erfolgreich abgerufen",
                    Toast.LENGTH_SHORT).show();
        }


    }

    /**
     * Wird aufgerufen, um das aktuelle Verhalten des Fragmentes auf den Wert der Variable debug anzupassen
     *
     * @return      void
     */
    private void debugToggle()
    {
        // Falls im Debugmodus, werden Testfelder/-Buttons angezeigt
        if(debug)
        {
            buttonDetentionTime = 250;
            v.findViewById(R.id.skip).setVisibility(View.VISIBLE);
            v.findViewById(R.id.debugFrame).setVisibility(View.VISIBLE);
            v.findViewById(R.id.debugRefresh).setVisibility(View.VISIBLE);
        }
        else
        {
            buttonDetentionTime = 1000;
            if(choosePosition) v.findViewById(R.id.skip).setVisibility(View.INVISIBLE);
            v.findViewById(R.id.debugFrame).setVisibility(View.INVISIBLE);
            v.findViewById(R.id.debugRefresh).setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void update(Observable observable, Object o) {

        if(debug) debugRefresh();

    }
}
