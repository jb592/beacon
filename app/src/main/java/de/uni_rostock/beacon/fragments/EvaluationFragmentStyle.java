package de.uni_rostock.beacon.fragments;


import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.List;
import java.util.Objects;

import de.uni_rostock.beacon.EvaluationController;
import de.uni_rostock.beacon.ILocalization;
import de.uni_rostock.beacon.Location;
import de.uni_rostock.beacon.MapData;
import de.uni_rostock.beacon.MapDataManager;
import de.uni_rostock.beacon.MyApplication;
import de.uni_rostock.beacon.Position;
import de.uni_rostock.beacon.R;
import de.uni_rostock.beacon.TestPlace;

/**
 * Created by Tom on 14.02.2016.
 */
public class EvaluationFragmentStyle extends Fragment{
    private View v;
    private Spinner spinner;
    private final List<MapData> maps = new MapDataManager().getMapDatas();
    private Fragment parent;

    public void setParent(Fragment parent) {
        this.parent = parent;
    }

    @Override
    public String getName() {
        return "Auswahl der Evaluationsart";
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        v = inflater.inflate(R.layout.fragment_evaluation_style, container, false);

        spinner = (Spinner) v.findViewById(R.id.map);
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<MapData> adapter = new ArrayAdapter<>(
                MyApplication.getActivity(), android.R.layout.simple_spinner_dropdown_item, maps);
        // Apply the adapter to the spinner
        spinner.setAdapter(adapter);

        v.findViewById(R.id.startRandom).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getActivity(), "Zufälliger Testlauf gestartet",
                        Toast.LENGTH_SHORT).show();
                EvaluationFragmentMain.testRunFragment.setMapData(maps.get(spinner.getSelectedItemPosition()).getName());
                EvaluationFragmentMain.testRunFragment.setChooseMode(false);

                MyApplication.getActivity().showFragment(EvaluationFragmentMain.testRunFragment);
            }
        });
        v.findViewById(R.id.startCustom).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getActivity(), "Benutzerdefinierter Testlauf gestartet",
                        Toast.LENGTH_SHORT).show();

                EvaluationFragmentMain.testRunFragment.setMapData(maps.get(spinner.getSelectedItemPosition()).getName());
                EvaluationFragmentMain.testRunFragment.setChooseMode(true);

                MyApplication.getActivity().showFragment(EvaluationFragmentMain.testRunFragment);
            }
        });
        return v;
    }

    @Override
    public boolean onBackButtonPress() {
        if (parent != null) {
            MyApplication.getActivity().showFragment(parent);
            return true;
        }
        return false;
    }
}
