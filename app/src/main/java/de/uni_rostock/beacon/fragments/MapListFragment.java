package de.uni_rostock.beacon.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.melnykov.fab.FloatingActionButton;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;

import de.uni_rostock.beacon.MapData;
import de.uni_rostock.beacon.MapDataManager;
import de.uni_rostock.beacon.MyApplication;
import de.uni_rostock.beacon.R;
import de.uni_rostock.beacon.adapters.MapsAdapter;

/**
 * @author Johann Bauer
 *         created 17.01.2016
 */
public class MapListFragment extends Fragment {
    private ArrayList<MapData> maps;
    private View v;
    public static MapDetailFragment detailFragment = new MapDetailFragment();
    private RecyclerView rv;
    private MapsAdapter adapter;
    private MapDataManager mapDataManager;

    // number of images to select
    private static final int PICK_IMAGE = 1;
    private static final int REQUEST_CAMERA = 2;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_maps_list, container, false);
        mapDataManager = new MapDataManager();
        rv = (RecyclerView)v.findViewById(R.id.rv);
        rv.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        rv.setLayoutManager(llm);

        adapter = new MapsAdapter(mapDataManager.getMapDatas());
        rv.setAdapter(adapter);

        FloatingActionButton fab = (FloatingActionButton) v.findViewById(R.id.fab);
        fab.attachToRecyclerView(rv);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final CharSequence[] items = { "Karte fotografieren", "Karte laden"};

                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("Neue Karte laden");
                builder.setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int item) {
                        if (items[item].equals("Karte fotografieren")) {
                            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            startActivityForResult(intent, REQUEST_CAMERA);
                        } else {
                            Intent intent = new Intent();
                            intent.setType("image/*");
                            intent.setAction(Intent.ACTION_GET_CONTENT);
                            startActivityForResult(Intent.createChooser(intent, "Neue Karte laden"), PICK_IMAGE);
                        }
                    }
                });
                builder.show();
            }




        });
        detailFragment.setParent(this);

        return v;
    }

    @Override
    public String getName() {
        return "Karten";
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if ((requestCode == PICK_IMAGE || requestCode == REQUEST_CAMERA) && resultCode == getActivity().RESULT_OK && data != null) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = { MediaStore.Images.Media.DATA };

            Cursor cursor = getActivity().getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            cursor.close();

            File image = new File(picturePath);
            String name = image.getName();
            int lastIndexOfPoint = name.lastIndexOf('.');

            if (lastIndexOfPoint != -1) {
                name = name.substring(0, lastIndexOfPoint);
            }

            name += ".png";

            String path = Environment.getExternalStorageDirectory() + "/beacon/maps/";
            File f = new File(path + name);

            try {
                f.createNewFile();
            } catch (IOException ex) {
                //TODO
                return;
            }

            if (f.exists()) {
                f.delete();
            }

            try {
                OutputStream fos = new FileOutputStream(f);
                Bitmap bitmap = BitmapFactory.decodeFile(picturePath);
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
                fos.close();
            } catch(FileNotFoundException ex) {
                //TODO
                return;

            } catch(IOException ex) {
                //TODO
                return;

            }

            MapsAdapter adapter = new MapsAdapter(mapDataManager.getMapDatas());
            rv.setAdapter(adapter);
        }
    }

    @Override
    public String getCategory() {
        return "Verwalten";
    }
}
