package de.uni_rostock.beacon.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.os.Bundle;
import android.view.DragEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;

import java.util.concurrent.TimeUnit;

import de.uni_rostock.beacon.MapData;
import de.uni_rostock.beacon.MapDataManager;
import de.uni_rostock.beacon.MyApplication;
import de.uni_rostock.beacon.R;
import de.uni_rostock.beacon.TestPlace;
import de.uni_rostock.beacon.TestRun;
import de.uni_rostock.beacon.bluetooth.LocationManager;
import de.uni_rostock.beacon.views.DrawingView;

/**
 * Created by Tom on 01.03.2016.
 */
public class EvaluationFragmentTestPlaces extends Fragment{
    private View v;
    private MapDataManager mapDataManager = new MapDataManager();
    private Fragment parent;
    private TextView name;
    private TextView date;
    private TextView realPosition;
    private TextView locaPosition;
    private SeekBar seekBar;
    private TestRun testRun;
    private MapData selectedMapData;
    private DrawingView mapDrawing;

    public void setParent(Fragment parent) {
        this.parent = parent;
    }

    /**
     * Sucht und setzt die Karte, auf der der zu betrachtende Testlauf stattgefunden hat
     *
     * @param  m  Name der Karte, auf der der Testlauf durchgeführt wurde
     * @return      Gibt zurück, ob die Karte erfolgreich geladen werden konnte
     */
    public boolean setMapData(String m)
    {
        selectedMapData = mapDataManager.getMapData(m);
        if(selectedMapData == null) return false;
        return true;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_evaluation_testplaces, container, false);
        onAfterCreateView(v);

        name = (TextView) v.findViewById(R.id.name);
        date = (TextView) v.findViewById(R.id.startDate);
        realPosition = (TextView) v.findViewById(R.id.realPosition);
        locaPosition = (TextView) v.findViewById(R.id.locaPosition);
        seekBar = (SeekBar) v.findViewById(R.id.seekBar);



        seekBar.setOnSeekBarChangeListener(
                new SeekBar.OnSeekBarChangeListener() {
                    @Override
                    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                        setTestPlace(progress);
                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {

                    }

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {

                    }
                }

        );

        mapDrawing = (DrawingView) v.findViewById(R.id.drawing_image);
        mapDrawing.setMode(DrawingView.Mode.DRAW_NONE);
        if(selectedMapData != null) mapDrawing.setMapData(selectedMapData);

        if(testRun != null) setTestRun(testRun);
        return v;
    }

    /**
     * Setzt den Testlauf, dessen Testorte in diesem Fragment angezeigt werden soll.
     *
     * @param  cur  Testlauf, der betrachtet werden soll
     * @return      void
     */
    public void setTestRun(TestRun cur) {
        this.testRun = cur;
        if (v != null) {
            if(testRun.getTestPlaces().size() == 0) return;
            setTestPlace(0);
        }
    }

    /**
     * Wird aufgerufen, um die Informationen über den entsprechenden Testort anzuzeigen.
     *
     *
     * @param  testPlaceNumber  Position des Testortes in der Liste, dessen Informationen angezeigt werden soll
     * @return      void
     */
    private void setTestPlace(int testPlaceNumber)
    {
        mapDrawing.clearRegionsToDraw();
        TestPlace tP = testRun.getTestPlaces().get(testPlaceNumber);

        name.setText("Testort " + tP.getNumber() + "/" + testRun.getTestPlaces().size());
        date.setText(tP.getDate().toString());
        String locations = "";
        for(String s: tP.getRealLocation())
        {
            locations += s + "\n";
            if(LocationManager.findLocationByName(s) != null)
                mapDrawing.addRegionToDraw(LocationManager.findLocationByName(s).getRegion(), s);
        }

        realPosition.setText(locations);
        locations = "";
        for(String s: tP.getLocaLocation())
        {
            locations += s + "\n";
            if(LocationManager.findLocationByName(s) != null)
                mapDrawing.addRegionToDraw(LocationManager.findLocationByName(s).getRegion(), s);
        }
        locaPosition.setText(locations);

        seekBar.setMax(testRun.getTestPlaces().size() - 1);
        seekBar.setProgress(testPlaceNumber);

        if(mapDrawing != null)
        {
            mapDrawing.setPositionToDraw(tP.getRealPosition());
            mapDrawing.setPositionToDraw2(tP.getLocaPosition());

        }
        // mapDrawing.setPositionToDraw(tP.getLocaPosition());


    }

    @Override
    public boolean onBackButtonPress() {
        if (parent != null) {
            MyApplication.getActivity().showFragment(parent);
            return true;
        }
        return false;
    }

    @Override
    public String getName() {
        return "Detailansicht der Testorte";
    }

}