package de.uni_rostock.beacon.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.melnykov.fab.FloatingActionButton;

import de.uni_rostock.beacon.Location;
import de.uni_rostock.beacon.MyApplication;
import de.uni_rostock.beacon.R;
import de.uni_rostock.beacon.adapters.LocationAdapter;
import de.uni_rostock.beacon.bluetooth.LocationManager;

/**
 * @author Johann Bauer
 *         created 17.01.2016
 */
public class LocationListFragment extends Fragment {
    private View v;
    public static LocationDetailFragment detailFragment = new LocationDetailFragment();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_location_list, container, false);
        onAfterCreateView(v);
        RecyclerView rv = (RecyclerView)v.findViewById(R.id.rv);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        rv.setLayoutManager(llm);

        LocationAdapter adapter = new LocationAdapter(LocationManager.listAll());
        rv.setAdapter(adapter);

        FloatingActionButton fab = (FloatingActionButton) v.findViewById(R.id.fab);
        fab.attachToRecyclerView(rv);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                detailFragment.setLocation(new Location());
                MyApplication.getActivity().showFragment(detailFragment);
            }
        });
        detailFragment.setParent(this);
        return v;
    }

    @Override
    public String getName() {
        return "Orte";
    }

    @Override
    public String getCategory() {
        return "Verwalten";
    }
}
