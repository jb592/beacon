package de.uni_rostock.beacon.fragments;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.Menu;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import de.uni_rostock.beacon.MyApplication;

/**
 * @author Johann Bauer
 *         created 21.02.2016
 */
public abstract class Fragment extends android.app.Fragment implements Comparable<Fragment> {

    public void onAfterCreateView(View v) {
        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm =  (InputMethodManager) MyApplication.getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
            }
        });
    }

    /**
     * Diese Methode wird aufgerufen, wenn der Nutzer den "Zurück"-Hardware-Button betätigt.
     * Verschachtelte Fragmente sollten diese Methode überschreiben und zum Elternfragment zurückkehren.
     * @see de.uni_rostock.beacon.MainActivity#showFragment
     * @return false, wenn die App selbst die Aktion behandeln soll, true sonst
     */
    public boolean onBackButtonPress() {
        return false;
    }

    /**
     * Gibt den Namen an, der im Menü und - bei aktiviertem Fragment - in der Toolbar angezeigt
     * werden soll.
     * @return anzuzeigender Name
     */
    public String getName() {
        return "";
    }

    /**
     * Gibt an, an welcher Stelle im Menü ein Fragment stehen soll. Geben zwei Fragmente die selbe
     * Zahl zurück, so wird die Reihenfolge der Fragmente mit demselben Index durch Zufall bestimmt.
     * @return Menü-Index
     */
    public Integer getMenuIndex() {
        return Integer.MAX_VALUE;
    }

    public String getCategory() {
        return "";
    }

    @Override
    public int compareTo(@NonNull Fragment another) {
        return getMenuIndex().compareTo(another.getMenuIndex());
    }

    /**
     * Soll das Menü für das Fragment angepasst werden, kann das hier gemacht werden.
     * @param m Menü
     */
    public void onInsertMenu(Menu m) {

    }

    /**
     * Wenn diese Methode true zurückgibt, wird beim Beenden des Fragments eine Sicherheitsabfrage eingeblendet.
     * @return
     */
    public boolean needsSave() {
        return false;
    }
}
