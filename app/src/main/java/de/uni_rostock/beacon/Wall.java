package de.uni_rostock.beacon;

import android.util.Log;

/**
 * Created by georgehrke on 2/18/16.
 */
public class Wall {

    private Position start;

    private Position end;

    public Wall() {
    }

    public Wall(Position start, Position end) {
        Log.d("wall", "constructing wall with start and end");
        this.start = start;
        this.end = end;
    }

    public Position getStart() {
        return start;
    }

    public void setStart(Position start) {
        this.start = start;
    }

    public Position getEnd() {
        return end;
    }

    public void setEnd(Position end) {
        this.end = end;
    }
}
