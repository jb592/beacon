package de.uni_rostock.beacon;


import com.orm.SugarRecord;

import java.util.Arrays;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Tom on 14.02.2016.
 */
public class TestPlace extends SugarRecord {

    String number;
    Date date = new Date();
    Position realPosition;
    Position locaPosition;
    String realLocation;
    String locaLocation;
    private TestRun run;
    boolean sameLocation = false;

    public TestPlace()
    {

    }


    public TestPlace(Position real, Position loca, List<String> realLoca, List<String> locaLoca, TestRun run)
    {
        this.run = run;
        this.number = "" + (run.getTestPlaces().size() + 1);
        realPosition = real;
        locaPosition = loca;
        realLocation = "";
        String delimiter = "";
        for (String i: realLoca) {
            realLocation += delimiter + i;
            delimiter = ",";
        }
        locaLocation = "";
        delimiter = "";
        for (String i: locaLoca) {
            locaLocation += delimiter + i;
            delimiter = ",";
        }
        if (realPosition != null)
            realPosition.save();
        if (locaPosition !=null)
            locaPosition.save();

        if(realLoca.size() != locaLoca.size())
        {
            sameLocation = false;
            return;
        }
        for(String k: realLoca)
        {
            sameLocation = false;
            for(String l: locaLoca)
            {
                if(k.equals(l))
                {
                    sameLocation = true;
                    break;
                }
            }
            if(sameLocation == false) break;
        }
    }

    public Date getDate()
    {
        return date;
    }

    public String getNumber()
    {
        return number;
    }

    public Position getRealPosition()
    {
        return realPosition;
    }

    public void setRealPosition(Position realPosition) {
        this.realPosition = realPosition;
    }

    public Position getLocaPosition() { return locaPosition; }

    public void setLocaPosition(Position locaPosition) {
        this.locaPosition = locaPosition;
    }

    public List<String> getRealLocation() {
        return new LinkedList<>(Arrays.asList(realLocation.split(",")));
    }



    public List<String> getLocaLocation() {
        return new LinkedList<>(Arrays.asList(locaLocation.split(",")));
    }


    public boolean isSameLocation() {
        return sameLocation;
    }

    public void setSameLocation(boolean sameLocation) {
        this.sameLocation = sameLocation;
    }

}
