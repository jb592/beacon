package de.uni_rostock.beacon;

import android.graphics.Region;
import android.support.annotation.NonNull;
import android.util.Log;

import com.orm.SugarRecord;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * Created by bauerj on 24.02.2016.
 */
public class Location extends SugarRecord implements Comparable<Location> {
    private String mapName;
    private byte[] region;
    private transient Region compiledRegion = null;
    private String name;
    private float scalingFactor = 0.0f;

    /**
     * Gibt den verwendeten Skalierungsfaktor der Region zurück. Beim Anzeigen des Ortes muss überprüft
     * werden, ob der Skalierungsfaktor übereinstimmt, andernfalls ist er mit Region.translate anzupassen
     * und zusammen mit der Region hier zu aktualisieren.
     * @see Region#translate(int, int)
     * @return Skalierungsfaktor > 0
     */
    public float getScalingFactor() {
        return scalingFactor;
    }

    public void setScalingFactor(float scalingFactor) {
        this.scalingFactor = scalingFactor;
    }

    /**
     * Liefert den Namen der Karte, auf der der Ort gezeichnet werden soll.
     * @return Kartenname
     */
    public String getMapName() {
        return mapName;
    }

    public void setMapName(String mapName) {
        this.mapName = mapName;
    }

    /**
     * Gibt die Region zurück, die die Abmessungen des Ortes beschreibt.
     * @return Region
     */
    public Region getRegion() {
        if (compiledRegion != null)
            return compiledRegion;
        ByteArrayInputStream bais = new ByteArrayInputStream(region);
        ObjectInputStream ois = null;
        Object o = null;
        try {
            ois = new ObjectInputStream(bais);
            o = ois.readObject();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        compiledRegion = (SerializableRegion)(o);
        return compiledRegion;
    }

    public void setRegion(SerializableRegion region) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectOutputStream oos = null;
        try {
            oos = new ObjectOutputStream(baos);
            oos.writeObject(region);
            oos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.region = baos.toByteArray();
        Log.d("RegionSize", this.region.length + "B");
        compiledRegion = region;
    }


    /**
     * Ermittelt, ob der Ort eine Region besitzt.
     * @return true|false
     */
    public boolean hasRegion() {
        return region != null;
    }

    /**
     * Gibt den Namen des Ortes zurück
     * @return Name des Ortes
     */
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int compareTo(@NonNull Location another) {
        return name.compareTo(another.name);
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Location))
            return false;
        return name.equals(((Location) o).name);
    }
}
