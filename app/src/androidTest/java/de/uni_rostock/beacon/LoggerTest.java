package de.uni_rostock.beacon;

import android.support.test.runner.AndroidJUnit4;

import org.junit.Test;
import org.junit.runner.RunWith;

import de.uni_rostock.beacon.bluetooth.Logger;

/**
 * @author Johann Bauer
 *         created 08.02.2016
 */
@RunWith(AndroidJUnit4.class)
public class LoggerTest {
    private static class TestClass {
        public String name;
        public String position;

        public TestClass(String name, String position) {
            this.name = name;
            this.position = position;
        }
    }

    @Test
    public void testLogging() {
        Logger t = new Logger(TestClass.class);
        t.log(new TestClass("Estimote", "Schlosstraße 7"));
        t.log(new TestClass("blukii", "Tür"));
        t.commit();
    }
}
